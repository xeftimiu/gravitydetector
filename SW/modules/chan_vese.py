import numpy as np
import scipy.ndimage as nd
import cv2 
import sys

import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter1d, convolve
from tqdm import tqdm

eps = np.finfo(float).eps
	
def prob_chanvese(I, init_mask, valid_mask, max_its=200, alpha=0.2, convert=True):
	'''
	Probabilistic Chan-Vese model used for loacting missing cells.
	Instead of matching interior and exterior means, two histograms are calculated
	and level set function derivatives are calculated based on these distributions.
	The implementation is based on the scikit-image Chan-Vese implementation
	and the theory is based on the work of Cremers et al (see: 10.1117/12.708609)
	'''

	I = I.astype(float)

	# Create a signed distance map (SDF) from mask
	if convert:
		phi = mask2phi(init_mask)
	else:
		phi = init_mask

	# Main loop
	its = 0
	stop = False
	prev_mask = init_mask
	c = 0
	total_energies = []
	min_energy = None
	while (its < max_its and not stop):
		# Get the curve's narrow band
		idx = np.flatnonzero(np.logical_and(phi <= 1.2, phi >= -1.2))
		phiidx = phi.flat[idx]
		if idx.shape[0] > 0:
			# Find interior and exterior mean
			upts = np.flatnonzero((phi <= 0) & valid_mask)  # interior points
			vpts = np.flatnonzero((phi > 0) & valid_mask)  # exterior points			
			upts2 = np.flatnonzero(phiidx <= 0)  # interior points
			vpts2 = np.flatnonzero(phiidx > 0)  # exterior points


			# Force from image information
			bins = np.linspace(0, 1, 256)
			
			int_hist, int_hist_bins = np.histogram(I.flat[upts], bins, density=True)
			u = np.sum(int_hist*np.linspace(0, 1, 255)) / (len(upts) + eps)  # interior mean
			int_hist = np.pad(int_hist * np.diff(int_hist_bins), (1, 1), 'constant', constant_values=(0, 0))
			ext_hist, ext_hist_bins = np.histogram(I.flat[vpts], bins, density=True)
			v = np.sum(ext_hist*np.linspace(0, 1, 255)) / (len(vpts) + eps)  # exterior mean
			ext_hist =  np.pad(ext_hist * np.diff(ext_hist_bins), (1, 1), 'constant', constant_values=(0, 0))
			
			int_hist = gaussian_filter1d(int_hist, 2)
			ext_hist = gaussian_filter1d(ext_hist, 2)
			
			int_hist = np.log(int_hist*255+1)
			ext_hist = np.log(ext_hist*255+1)
			
			int_hist = int_hist/(np.max(int_hist)) # Always make the largest prob = 1
			ext_hist = ext_hist/(np.max(ext_hist)) # This way, multimodal distributions don't get any weaker as time goes on 

			probs = np.digitize(I.flat[idx], bins)
			int_probs = np.digitize(I.flat[idx][upts2], bins)
			ext_probs = np.digitize(I.flat[idx][vpts2], bins)
			
			
			F = np.empty_like(idx, float)
			F[vpts2] = ext_hist[ext_probs] - int_hist[ext_probs] # If similar to the background, go up, if similar to the foreground, go down
			F[upts2] = 2*ext_hist[int_probs] - 1 # = ext_hist[int_probs] - (1 - ext_hist[int_probs]) -> if similar to the BG, go up, if dissimilar to the BG, go down
			F += 5*((I.flat[idx] - u)**2 - (I.flat[idx] - v)**2)
			
			# Force from curvature penalty
			curvature = get_curvature(phi, idx)

			# Gradient descent to minimize energy
			dphidt = F / np.max(np.abs(F)) + alpha * curvature

			# Maintain the CFL condition
			dt = 0.45 / (np.max(np.abs(dphidt)) + eps)

			# Evolve the curve
			phi.flat[idx] += dt * dphidt

			# Keep SDF smooth
			phi = sussman(phi, 0.5)

			its = its + 1
			if its > max_its:
				break
		else:
			#print('Empty narrow band')
			break


	# Make mask from SDF
	seg = phi <= 0  # Get mask from levelset

	return seg, phi, its

# ---------------------------------------------------------------------
# ---------------------- AUXILIARY FUNCTIONS --------------------------
# ---------------------------------------------------------------------

def bwdist(a):
	"""
	Intermediary function. 'a' has only True/False vals,
	so we convert them into 0/1 values - in reverse.
	True is 0, False is 1, distance_transform_edt wants it that way.
	"""
	return nd.distance_transform_edt(a == 0)


# Displays the image with curve superimposed
def show_curve_and_phi(fig, I, phi, color):
	fig.axes[0].cla()
	fig.axes[0].imshow(I, cmap='gray')
	fig.axes[0].contour(phi, 0, colors=color)
	fig.axes[0].set_axis_off()
	plt.draw()

	fig.axes[1].cla()
	fig.axes[1].imshow(phi)
	fig.axes[1].set_axis_off()
	plt.draw()
	
	plt.pause(0.001)


def im2double(a):
	a = a.astype(float)
	a /= np.abs(a).max()
	return a


# Converts a mask to a SDF
def mask2phi(init_a):
	phi = bwdist(init_a) - bwdist(1 - init_a) + im2double(init_a) - 0.5
	return phi


# Compute curvature along SDF
def get_curvature(phi, idx):
	dimy, dimx = phi.shape
	yx = np.array([np.unravel_index(i, phi.shape) for i in idx])  # subscripts
	y = yx[:, 0]
	x = yx[:, 1]

	# Get subscripts of neighbors
	ym1 = y - 1
	xm1 = x - 1
	yp1 = y + 1
	xp1 = x + 1

	# Bounds checking
	ym1[ym1 < 0] = 0
	xm1[xm1 < 0] = 0
	yp1[yp1 >= dimy] = dimy - 1
	xp1[xp1 >= dimx] = dimx - 1

	# Get indexes for 8 neighbors
	idup = np.ravel_multi_index((yp1, x), phi.shape)
	iddn = np.ravel_multi_index((ym1, x), phi.shape)
	idlt = np.ravel_multi_index((y, xm1), phi.shape)
	idrt = np.ravel_multi_index((y, xp1), phi.shape)
	idul = np.ravel_multi_index((yp1, xm1), phi.shape)
	idur = np.ravel_multi_index((yp1, xp1), phi.shape)
	iddl = np.ravel_multi_index((ym1, xm1), phi.shape)
	iddr = np.ravel_multi_index((ym1, xp1), phi.shape)

	# Get central derivatives of SDF at x,y
	phi_x = -phi.flat[idlt] + phi.flat[idrt]
	phi_y = -phi.flat[iddn] + phi.flat[idup]
	phi_xx = phi.flat[idlt] - 2 * phi.flat[idx] + phi.flat[idrt]
	phi_yy = phi.flat[iddn] - 2 * phi.flat[idx] + phi.flat[idup]
	phi_xy = 0.25 * (- phi.flat[iddl] - phi.flat[idur] +
					 phi.flat[iddr] + phi.flat[idul])
	phi_x2 = phi_x**2
	phi_y2 = phi_y**2

	# Compute curvature (Kappa)
	curvature = ((phi_x2 * phi_yy + phi_y2 * phi_xx - 2 * phi_x * phi_y * phi_xy) /
				 (phi_x2 + phi_y2 + eps) ** 1.5) * (phi_x2 + phi_y2) ** 0.5

	return curvature


def sussman(D, dt):
	# forward/backward differences
	a = np.pad(D[:, 1:] - D[:, :-1], ((0, 0), (1, 0)), mode='constant', constant_values=0)
	b = np.pad(D[:, 1:] - D[:, :-1], ((0, 0), (0, 1)), mode='constant', constant_values=0)
	c = np.pad(D[:-1, :] - D[1:, :], ((0, 1), (0, 0)), mode='constant', constant_values=0)
	d = np.pad(D[:-1, :] - D[1:, :], ((1, 0), (0, 0)), mode='constant', constant_values=0)

	a_p = np.clip(a, 0, np.inf)
	a_n = np.clip(a, -np.inf, 0)
	b_p = np.clip(b, 0, np.inf)
	b_n = np.clip(b, -np.inf, 0)
	c_p = np.clip(c, 0, np.inf)
	c_n = np.clip(c, -np.inf, 0)
	d_p = np.clip(d, 0, np.inf)
	d_n = np.clip(d, -np.inf, 0)

	dD = np.zeros_like(D)
	D_neg_ind = np.flatnonzero(D < 0)
	D_pos_ind = np.flatnonzero(D > 0)

	dD.flat[D_pos_ind] = np.sqrt(
		np.max(np.concatenate(
			([a_p.flat[D_pos_ind]**2], [b_n.flat[D_pos_ind]**2])), axis=0) +
		np.max(np.concatenate(
			([c_p.flat[D_pos_ind]**2], [d_n.flat[D_pos_ind]**2])), axis=0)) - 1
	dD.flat[D_neg_ind] = np.sqrt(
		np.max(np.concatenate(
			([a_n.flat[D_neg_ind]**2], [b_p.flat[D_neg_ind]**2])), axis=0) +
		np.max(np.concatenate(
			([c_n.flat[D_neg_ind]**2], [d_p.flat[D_neg_ind]**2])), axis=0)) - 1

	D = D - dt * (D / np.sqrt(D**2 + 1)) * dD
	return D

