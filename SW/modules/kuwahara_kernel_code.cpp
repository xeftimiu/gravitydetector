#define M_PI 3.14159265358979323846
#define MIN(a,b) ( a < b ? a : b )
#define MAX(a,b) ( a > b ? a : b )
#define CROSS( ax , ay , bx , by ) ((ax) * (by) - (ay) * (bx))
//1/255*1/255 = 0.0000153787
#define EPS 0.00001 

__device__ float get_value_channel(const unsigned char* texture, uint64_t idx){
	return (0.299*static_cast<float>(texture[idx*3]) + 0.587*static_cast<float>(texture[idx*3+1]) + 0.114*static_cast<float>(texture[idx*3+2]));
}

///Caculate the structure tensor in 2D
__global__ void structure_tensor_2D(unsigned char* texture, float* output, int w, int h){
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	int y = idx / w;
	int x = idx % w;
	uint64_t tex_idx = y*w+x;
	uint64_t out_idx = tex_idx*3;
	float xder = 0.0; float yder = 0.0;
	//Second order central differences give inaccurate results for some reason...
	if(x >= 2 && x < w-2) xder = get_value_channel(texture, tex_idx-2)/12 - 2*get_value_channel(texture, tex_idx-1)/3 + 2*get_value_channel(texture, tex_idx+1)/3 - get_value_channel(texture, tex_idx+2)/12;
	//if(x >= 1 && x < w-1) xder = get_value_channel(texture, tex_idx+1)/2 - get_value_channel(texture, tex_idx-1)/2;
	if(y >= 2 && y < h-2) yder = get_value_channel(texture, (y-2)*w+x)/12 - 2*get_value_channel(texture, (y-1)*w+x)/3 + 2*get_value_channel(texture, (y+1)*w+x)/3 - get_value_channel(texture, (y+2)*w+x)/12;	
	//if(y >= 1 && y < h-1) yder = get_value_channel(texture, (y+1)*w+x)/2 - get_value_channel(texture, (y-1)*w+x)/2;

	output[out_idx] = xder*xder; // 0 - +-255^2
	output[out_idx+1] = xder*yder;
	output[out_idx+2] = yder*yder;
}

///Naive convolution is faster than cuFFT for some reason, possibly the low workload.
__global__ void convolve_2D(float* S, float* output, float* kernel, int16_t ksize, int w, int h, bool dim1, bool dim2){
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	int y = idx / w;
	int x = idx % w;
	uint64_t tex_idx = y*w+x;
	float acc1 = 0.0;
	float acc2 = 0.0;
	float acc3 = 0.0;
	for(int i = -ksize; i <= ksize; ++i){
		int16_t row = y+i*dim2;
		int16_t col = x+i*dim1;
		if(row < 0) row = -row;
		if(row >= h) row = h-(row-h+1);
		if(col < 0) col = -col;
		if(col >= w) col = w-(col-w+1);
		uint64_t ch_idx = row*w+col;
		float coeff = kernel[i+ksize];
		acc1 += S[ch_idx*3] * coeff;
		acc2 += S[ch_idx*3+1] * coeff;
		acc3 += S[ch_idx*3+2] * coeff;
	}
	output[tex_idx*3] = acc1;
	output[tex_idx*3+1] = acc2;
	output[tex_idx*3+2] = acc3;
}

///Caculate the eigenvectors of the structure tensor
__device__ void calc_eigs_2D(float* S, float& anisotropy, float& phi, uint64_t idx){
	uint64_t S_idx = idx*3;
	float E = S[S_idx];
	float F = S[S_idx+1];
	float G = S[S_idx+2];
	float EminG = E-G;
	float EplusG = E+G;
	float det = sqrt(EminG*EminG + 4*F*F);
	float lambda1 = (EplusG + det)/2;
	float lambda2 = (EplusG - det)/2;
	if((lambda1 + lambda2) > EPS){
		anisotropy = (lambda1-lambda2)/(lambda1 + lambda2);
		phi = atan2(-F, lambda1-E);				
	}
}

//Perform Kuwahara filtering in 2D
__global__ void run_kuwahara_2D(unsigned char* texture, unsigned char* output, int w, int h, float* S, int N, float r, float gauss_base, float xi, float eta, float alpha=1.0, int q=8){
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	int y = idx / w;
	int x = idx % w;
	uint64_t tex_idx = y*w+x;
	uint64_t out_idx = tex_idx*3;
	float ani = 0.0; float phi = 0.0;
	calc_eigs_2D(S, ani, phi, tex_idx);
	float A = r*(alpha + ani)/alpha;
	float B = r*alpha/(alpha+ani);
	float sp = sin(phi); float cp = cos(phi);
	int16_t bound_u = ceil(sqrt(A*A*cp*cp + B*B*sp*sp));
	int16_t bound_v = ceil(sqrt(A*A*sp*sp + B*B*cp*cp));
	
	float region_w_sum = 0.0;
	float region_wr_sum = 0.0;
	float region_wg_sum = 0.0;
	float region_wb_sum = 0.0;
	for(uint8_t ti = 0; ti < N; ++ti){
		float theta = 2*ti*M_PI/N;
		float st = sin(theta); float ct = cos(theta);
		float sumr = 0.0; float sumg = 0.0; float sumb = 0.0;
		float sq_sumr = 0.0; float sq_sumg = 0.0; float sq_sumb = 0.0;
		float w_sum = 0.0;
		int16_t v_start = MAX(-bound_v, -y);
		int16_t v_end = MIN(bound_v, h-1-y);
		int16_t u_start = MAX(-bound_u, -x);
		int16_t u_end = MIN(bound_u, w-1-x);
		for(int16_t v = v_start; v <= v_end; ++v){
			for(int16_t u = u_start; u <= u_end; ++u){
				int img_idx = ((y+v)*w+x+u)*3;
				float U = u*((cp*ct)/A - (sp*st)/B) - v*((ct*sp)/A + (cp*st)/B);
				float V = u*((cp*st)/A + (ct*sp)/B) + v*((cp*ct)/B - (sp*st)/A);
				float etaVV = eta * V * V;
				bool inside = U >= etaVV - xi;
				if(!inside){
					continue;
				}
				float sqrtw = (U+xi - etaVV);
				float w = sqrtw * sqrtw * exp(gauss_base*(U*U + V*V));
				float wr = w*texture[img_idx]; 
				float wg = w*texture[img_idx+1]; 
				float wb = w*texture[img_idx+2];
				float wrr = wr*texture[img_idx]; 
				float wgg = wg*texture[img_idx+1]; 
				float wbb = wb*texture[img_idx+2];
				sumr += wr; sumg += wg; sumb += wb;
				sq_sumr += wrr; sq_sumg += wgg; sq_sumb += wbb; 
				w_sum += w;
			}
		}
		float meanr = sumr/w_sum;
		float meang = sumg/w_sum;
		float meanb = sumb/w_sum;
		float varr = sq_sumr/w_sum - meanr*meanr;
		float varg = sq_sumg/w_sum - meang*meang;
		float varb = sq_sumb/w_sum - meanb*meanb;
		float var = varr + varg + varb;
		float multiplier = 1/(1+pow(var, q/2));
		region_w_sum += multiplier;
		region_wr_sum += meanr*multiplier;
		region_wg_sum += meang*multiplier;
		region_wb_sum += meanb*multiplier;
	}
	output[out_idx] = static_cast<unsigned char>(region_wr_sum/region_w_sum);
	output[out_idx+1] = static_cast<unsigned char>(region_wg_sum/region_w_sum);
	output[out_idx+2] = static_cast<unsigned char>(region_wb_sum/region_w_sum);
}