#include <stdio.h>
#include <vector>
#include <algorithm>
#include <stdint.h>
#include "mask_extraction.h"

struct Heap_elem{
	uint16_t idx=0;
	uint8_t value=0;
	uint64_t loc=0;
};

/*bool operator<(Heap_elem a, Heap_elem b){
	if(a.idx != b.idx){
		return a.idx > b.idx;
	}else{
		return a.value < b.value;
	}
}*/

bool operator<(Heap_elem a, Heap_elem b){
	if(a.idx != b.idx){
		return a.idx > b.idx;
	}else if(a.value != b.value){
		return a.value < b.value;	
	}else{
		return a.loc < b.loc;
	}
}

// Mask extraction algorithm that uses the original image for contrast calculation.
void extract_mask(uint8_t* img, int16_t* set, uint8_t* walls, uint64_t* locs, uint64_t* neighs, uint32_t num_pts, uint8_t num_neighs, float wall_thresh, float halve_tresh){
	std::vector<Heap_elem> heap;
	for(size_t i = 0; i < num_pts; ++i){
		heap.push_back(Heap_elem{.idx=(uint16_t)(i+1), .value=img[locs[i]], .loc=locs[i]});
		std::push_heap(heap.begin(), heap.end());
	}
	uint16_t curr_idx = 1;
	uint64_t curr_sum = 0;
	uint64_t curr_count = 0;
	float avg = 0;
	while(heap.size() > 0){
		std::pop_heap(heap.begin(), heap.end());
		Heap_elem curr = heap.back();
		//printf("Elem: %d %d", curr.idx, curr.value);
		heap.pop_back();
		if(set[curr.loc] > 0 || walls[curr.loc] != 0){
			continue;
		}
		if(curr_idx != curr.idx){
			curr_idx = curr.idx;
			curr_sum = 0;
			curr_count = 0;
		}
		set[curr.loc] = curr.idx;
		curr_sum += img[curr.loc];
		curr_count += 1;
		avg = (float)curr_sum/(float)curr_count;
		float wt = wall_thresh;
		if(avg < halve_tresh){
			wt /= 2;
		}
		if(avg*(1-avg*wt) > img[curr.loc]){
			walls[curr.loc] = 1;
			set[curr.loc] = 0;
		}else{
			for(size_t n = 0; n < num_neighs; ++n){
				uint64_t n_loc = neighs[curr.loc*num_neighs+n];
				if(set[n_loc] == 0 && walls[n_loc] == 0){
					heap.push_back(Heap_elem{.idx=curr.idx, .value=img[n_loc], .loc=n_loc});
					std::push_heap(heap.begin(), heap.end());
					set[n_loc] = -1;
				}
			}
		}
	}
}

// Mask extraction algorithm that uses a separate, eroded image for the contrast calculation instead of the original image. 
void extract_mask_er(uint8_t* img, uint8_t* er_img, int16_t* set, uint8_t* walls, uint64_t* locs, uint64_t* neighs, uint32_t num_pts, uint8_t num_neighs, float wall_thresh){
	std::vector<Heap_elem> heap;
	for(size_t i = 0; i < num_pts; ++i){
		heap.push_back(Heap_elem{.idx=(uint16_t)(i+1), .value=img[locs[i]], .loc=locs[i]});
		std::push_heap(heap.begin(), heap.end());
	}
	uint16_t curr_idx = 1;
	uint64_t curr_sum = 0;
	uint64_t curr_count = 0;
	float avg = 0;
	while(heap.size() > 0){
		std::pop_heap(heap.begin(), heap.end());
		Heap_elem curr = heap.back();
		//printf("Elem: %d %d", curr.idx, curr.value);
		heap.pop_back();
		if(set[curr.loc] > 0 || walls[curr.loc] != 0){
			continue;
		}
		if(curr_idx != curr.idx){
			curr_idx = curr.idx;
			curr_sum = 0;
			curr_count = 0;
		}
		set[curr.loc] = curr.idx;
		curr_sum += img[curr.loc];
		curr_count += 1;
		avg = (float)curr_sum/(float)curr_count;
		if(avg*(1-avg*wall_thresh) > er_img[curr.loc]){
			walls[curr.loc] = 1;
			set[curr.loc] = 0;
		}else{
			for(size_t n = 0; n < num_neighs; ++n){
				uint64_t n_loc = neighs[curr.loc*num_neighs+n];
				if(set[n_loc] == 0 && walls[n_loc] == 0){
					heap.push_back(Heap_elem{.idx=curr.idx, .value=img[n_loc], .loc=n_loc});
					std::push_heap(heap.begin(), heap.end());
					set[n_loc] = -1;
				}
			}
		}
	}
}