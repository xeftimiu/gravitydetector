#ifdef _MSC_VER
    #define EXPORT_SYMBOL __declspec(dllexport)
#else
    #define EXPORT_SYMBOL
#endif

EXPORT_SYMBOL extern "C" void explicit_euler(double* Ax, double* Ay, double* Bx, double* By, double* Cx, double* Cy, double* Dx, double* Dy, double* xs_global, double* ys_global, size_t num_pts, double step, size_t imshape_x, size_t imshape_y, bool ascent, size_t num_ts);

EXPORT_SYMBOL extern "C" void euler_heun(double* Ax, double* Ay, double* Bx, double* By, double* Cx, double* Cy, double* Dx, double* Dy, double* xs_global, double* ys_global, size_t num_pts, double global_step, size_t imshape_x, size_t imshape_y, bool ascent, double tol, size_t num_ts);

EXPORT_SYMBOL extern "C" void EH_rasterize(double* Ax, double* Ay, double* Bx, double* By, double* Cx, double* Cy, double* Dx, double* Dy, double* xs_global, double* ys_global, size_t num_pts, double global_step, size_t imshape_x, size_t imshape_y, bool ascent, double tol, uint8_t* ws_lines, size_t num_ts);