#ifdef _MSC_VER
    #define EXPORT_SYMBOL __declspec(dllexport)
#else
    #define EXPORT_SYMBOL
#endif

EXPORT_SYMBOL extern "C" void extract_mask(uint8_t* img, int16_t* set, uint8_t* walls, uint64_t* locs, uint64_t* neighs, uint32_t num_pts, uint8_t num_neighs, float wall_thresh, float halve_tresh);
EXPORT_SYMBOL extern "C" void extract_mask_er(uint8_t* img, uint8_t* er_img, int16_t* set, uint8_t* walls, uint64_t* locs, uint64_t* neighs, uint32_t num_pts, uint8_t num_neighs, float wall_thresh);