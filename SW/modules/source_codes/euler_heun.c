#include <stdio.h>
#include <vector>
#include <algorithm>
#include <stdint.h>
#include <math.h>
#include <limits>
#include "euler_heun.h"

void explicit_euler(double* Ax, double* Ay, double* Bx, double* By, double* Cx, double* Cy, double* Dx, double* Dy, double* global_xs, double* global_ys, size_t num_pts, double step, size_t imshape_x, size_t imshape_y, bool ascent, size_t num_ts){
	int8_t dir = 1-2*ascent;
	for(size_t i = 0; i < num_pts; ++i){
		double x = global_xs[i];
		double y = global_ys[i];
		for(size_t t = 1; t <= num_ts; ++t){
				if(x < 0.0 || x > (imshape_x-1.0) || y < 0.0 || y > (imshape_y-1.0)){
					break;
				}
				double left, top;
				double xmod = modf(x, &left);
				double ymod = modf(y, &top);
				//size_t idx = top*(imshape_y-1)+left;
				size_t idx = left*(imshape_y-1)+top;
				double xymod = xmod*ymod;
				double x_avg = Ax[idx] + Bx[idx]*ymod + Cx[idx]*xmod + Dx[idx]*xymod;
				double y_avg = Ay[idx] + By[idx]*ymod + Cy[idx]*xmod + Dy[idx]*xymod;
				
				x += y_avg*step*dir;
				y += x_avg*step*dir;
		}
		global_xs[i] = x;
		global_ys[i] = y;
	}
}

void euler_heun(double* Ax, double* Ay, double* Bx, double* By, double* Cx, double* Cy, double* Dx, double* Dy, double* global_xs, double* global_ys, size_t num_pts, double global_step, size_t imshape_x, size_t imshape_y, bool ascent, double tol, size_t num_ts){
	int8_t dir = 1-2*ascent;
	for(size_t i = 0; i < num_pts; ++i){
		double x = global_xs[i];
		double y = global_ys[i];
		double step = global_step;
		for(size_t t = 1; t <= num_ts; ++t){
				if(x < 0.0 || x > (imshape_x-1.5) || y < 0.0 || y > (imshape_y-1.5)){
					break;
				}
				double left, top;
				double xmod = modf(x, &left);
				double ymod = modf(y, &top);
				size_t idx = left*(imshape_y-1)+top;
				double xymod = xmod*ymod;
				double k1x = Ax[idx] + Bx[idx]*ymod + Cx[idx]*xmod + Dx[idx]*xymod;
				double k1y = Ay[idx] + By[idx]*ymod + Cy[idx]*xmod + Dy[idx]*xymod;
				for(size_t it = 0; it < 20; ++it){
					double tx = x+k1y*step*dir;
					double ty = y+k1x*step*dir;
					if(tx < 0.0 || tx > (imshape_x-1.5) || ty < 0.0 || ty > (imshape_y-1.5)){
						break;
					}
					double lefth, toph;
					double xmodh = modf(tx, &lefth);
					double ymodh = modf(ty, &toph);
					double xymodh = xmodh*ymodh;
					size_t idxh = lefth*(imshape_y-1)+toph;

					double k2x = Ax[idxh] + Bx[idxh]*ymodh + Cx[idxh]*xmodh + Dx[idxh]*xymodh;
					double k2y = Ay[idxh] + By[idxh]*ymodh + Cy[idxh]*xmodh + Dy[idxh]*xymodh;
					double k12x = (k1x+k2x)/2;
					double k12y = (k1y+k2y)/2;
					double steph = step/2;
					
					double tx2 = x+k12y*steph*dir;
					double ty2 = y+k12x*steph*dir;
					if(tx2 < 0.0 || tx2 > (imshape_x-1.5) || ty2 < 0.0 || ty2 > (imshape_y-1.5)){
						break;
					}
					double lefth2, toph2;
					double xmodh2 = modf(tx2, &lefth2);
					double ymodh2 = modf(ty2, &toph2);
					double xymodh2 = xmodh2*ymodh2;
					size_t idxh2 = lefth2*(imshape_y-1)+toph2;
					double k3x = Ax[idxh2] + Bx[idxh2]*ymodh2 + Cx[idxh2]*xmodh2 + Dx[idxh2]*xymodh2;
					double k3y = Ay[idxh2] + By[idxh2]*ymodh2 + Cy[idxh2]*xmodh2 + Dy[idxh2]*xymodh2;
					double errx = (2/3)*abs(k12x-k3x);
					double erry = (2/3)*abs(k12y-k3y);
					double fac = tol/(std::max(errx, erry)+tol/100);
					if(fac > 1.0){
						x += k12y*step*dir;
						y += k12x*step*dir;
						break;
					}
					step = 0.9*step*std::max(0.5, std::min(fac, 2.0));
				}
		}
		global_xs[i] = x;
		global_ys[i] = y;
	}
}


void EH_rasterize(double* Ax, double* Ay, double* Bx, double* By, double* Cx, double* Cy, double* Dx, double* Dy, double* global_xs, double* global_ys, size_t num_pts, double global_step, size_t imshape_x, size_t imshape_y, bool ascent, double tol, uint8_t* ws_lines, size_t num_ts){
	int8_t dir = 1-2*ascent;
	for(size_t i = 0; i < num_pts; ++i){
		double x = global_xs[i];
		double y = global_ys[i];
		double step = global_step;
		for(size_t t = 1; t <= num_ts; ++t){
				if(x < 0.0 || x > (imshape_x-1.5) || y < 0.0 || y > (imshape_y-1.5)){
					break;
				}
				double left, top;
				double xmod = modf(x, &left);
				double ymod = modf(y, &top);
				uint32_t leftoff = std::floor(x+0.5);
				uint32_t topoff = std::floor(y+0.5);
				ws_lines[leftoff*imshape_y+topoff] = 1;
				
				size_t idx = left*(imshape_y-1)+top;
				double xymod = xmod*ymod;
				double k1x = Ax[idx] + Bx[idx]*ymod + Cx[idx]*xmod + Dx[idx]*xymod;
				double k1y = Ay[idx] + By[idx]*ymod + Cy[idx]*xmod + Dy[idx]*xymod;
				for(size_t it = 0; it < 20; ++it){
					double tx = x+k1y*step*dir;
					double ty = y+k1x*step*dir;
					if(tx < 0.0 || tx > (imshape_x-1.5) || ty < 0.0 || ty > (imshape_y-1.5)){
						break;
					}
					double lefth, toph;
					double xmodh = modf(tx, &lefth);
					double ymodh = modf(ty, &toph);
					double xymodh = xmodh*ymodh;
					size_t idxh = lefth*(imshape_y-1)+toph;
					double k2x = Ax[idxh] + Bx[idxh]*ymodh + Cx[idxh]*xmodh + Dx[idxh]*xymodh;
					double k2y = Ay[idxh] + By[idxh]*ymodh + Cy[idxh]*xmodh + Dy[idxh]*xymodh;
					double k12x = (k1x+k2x)/2;
					double k12y = (k1y+k2y)/2;
					double steph = step/2;
					
					double tx2 = x+k12y*steph*dir;
					double ty2 = y+k12x*steph*dir;
					if(tx2 < 0.0 || tx2 > (imshape_x-1.5) || ty2 < 0.0 || ty2 > (imshape_y-1.5)){
						break;
					}
					double lefth2, toph2;
					double xmodh2 = modf(tx2, &lefth2);
					double ymodh2 = modf(ty2, &toph2);
					double xymodh2 = xmodh2*ymodh2;
					size_t idxh2 = lefth2*(imshape_y-1)+toph2;
					double k3x = Ax[idxh2] + Bx[idxh2]*ymodh2 + Cx[idxh2]*xmodh2 + Dx[idxh2]*xymodh2;
					double k3y = Ay[idxh2] + By[idxh2]*ymodh2 + Cy[idxh2]*xmodh2 + Dy[idxh2]*xymodh2;
					double errx = (2/3)*abs(k12x-k3x);
					double erry = (2/3)*abs(k12y-k3y);
					double fac = tol/(std::max(errx, erry)+tol/100);
					if(fac > 1.0){
						double new_x = x + k12y*step*dir;
						double new_y = y + k12x*step*dir;
						int32_t leftoff2 = std::floor(new_x+0.5);
						int32_t topoff2 = std::floor(new_y+0.5);
						int32_t change = std::abs((int64_t)leftoff2-(int64_t)leftoff) + std::abs((int64_t)topoff2-(int64_t)topoff);
						if(change > 1){
							// p1 = (ax1, ay1) = (x, y)
							// p2 = (ax2, ay2) = (new_x, new_y)
							double ax1 = x;
							double ax2 = new_x;
							double ay1 = y;
							double ay2 = new_y;
							if(ax1 > ax2){
								std::swap(ax1, ax2);
								std::swap(ay1, ay2);
							}
							double miny = std::min(ay1, ay2);
							double maxy = std::max(ay1, ay2);
							if(ax1 < -0.5 || miny < -0.5){
								double dX = ax2-ax1;
								double dY = maxy-miny;
								double dist_to_x = (dX != 0 && ax1 < -0.5) ? (-0.5-ax1)/dX : 0.0; 
								double dist_to_y = (dY != 0 && miny < -0.5) ? (-0.5-miny)/dY : 0.0;
								ax1 = std::max(dist_to_x, dist_to_y)*dX + ax1;
								miny = std::max(dist_to_x, dist_to_y)*dY + miny;
							}
							
							if(ay1 < ay2){
								ay1 = miny;
							}else{
								ay2 = miny;
							}
							
							if(ax2 > imshape_x-0.50001 || maxy > imshape_y-0.50001){
								double dX = ax2-ax1;
								double dY = maxy-miny;
								double dist_to_x = (dX != 0 && ax2 > imshape_x-0.50001) ? (imshape_x-0.50001-ax1)/dX : std::numeric_limits<double>::max(); 
								double dist_to_y = (dY != 0 && maxy > imshape_y-0.50001) ? (imshape_y-0.50001-miny)/dY : std::numeric_limits<double>::max();
								ax2 = std::min(dist_to_x, dist_to_y)*dX + ax1;
								maxy = std::min(dist_to_x, dist_to_y)*dY + miny;
							}							
							if(ay1 > ay2){
								ay1 = maxy;
							}else{
								ay2 = maxy;
							}
							
							double X = ax1;
							double Y = ay1;
							int32_t X2 = (int32_t)std::floor(ax2+0.5);
							int32_t Y2 = (int32_t)std::floor(ay2+0.5);
							double dX = ax2-ax1;
							double dY = ay2-ay1;
							int8_t stepx = 1;
							int8_t stepy = (ay2>ay1) ? 1 : -1;
							double tMaxX, tDeltaX, tMaxY, tDeltaY;
							if(dX != 0.0){
								tMaxX = std::abs(((std::floor(ax1+0.5) + stepx*0.5)-ax1)/dX);
								tDeltaX = std::abs(1/dX);
							}else{
								tMaxX = std::numeric_limits<double>::max();
								tDeltaX = std::numeric_limits<double>::max();								
							}
							
							if(dY != 0.0){
								tMaxY = std::abs(((std::floor(ay1+0.5) + stepy*0.5)-ay1)/dY);
								tDeltaY = std::abs(1/dY);
							}else{
								tMaxY = std::numeric_limits<double>::max();
								tDeltaY = std::numeric_limits<double>::max();								
							}
							
							while(true){
								//printf("%4.2f %4.2f %4.2f %4.2f %4.2f %4.2f %d %d\n", ax1, ax2, ay1, ay2, X, Y, X2, Y2);
								if(tMaxX < tMaxY){
									tMaxX += tDeltaX;
									X += stepx;
								}else{
									tMaxY += tDeltaY;
									Y += stepy;									
								}
								if(X < -0.5 || X > imshape_x-0.5 || Y < -0.5 || Y > imshape_y-0.5){
									break;
								}
								
								int32_t xo = (int32_t)std::floor(X+0.5);
								int32_t yo = (int32_t)std::floor(Y+0.5);
								ws_lines[xo*imshape_y+yo] = 1;
								if(xo == X2 && yo == Y2){
									break;
								}
							}
						}
						x = new_x;
						y = new_y;
						break;
					}
					step = 0.9*step*std::max(0.5, std::min(fac, 2.0));
				}
		}
		global_xs[i] = x;
		global_ys[i] = y;
	}
}