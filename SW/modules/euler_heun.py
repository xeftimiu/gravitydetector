import pathlib
import ctypes
import numpy as np
import sys

if sys.platform == 'win32':
	libname = pathlib.Path('./modules/euler_heun.dll').absolute()
elif sys.platform == 'linux':
	libname = pathlib.Path('./modules/euler_heun.so').absolute()
else:
	print(f'OS type {sys.platform} not supported!')
	exit(1)
	
c_lib = ctypes.CDLL(str(libname.resolve()), winmode=0)

c_lib.explicit_euler.argtypes = [
np.ctypeslib.ndpointer(dtype=np.float64, ndim=2, flags='C_CONTIGUOUS'), 	#Ax
np.ctypeslib.ndpointer(dtype=np.float64, ndim=2, flags='C_CONTIGUOUS'), 	#Ay
np.ctypeslib.ndpointer(dtype=np.float64, ndim=2, flags='C_CONTIGUOUS'), 	#By
np.ctypeslib.ndpointer(dtype=np.float64, ndim=2, flags='C_CONTIGUOUS'), 	#By
np.ctypeslib.ndpointer(dtype=np.float64, ndim=2, flags='C_CONTIGUOUS'), 	#Cx
np.ctypeslib.ndpointer(dtype=np.float64, ndim=2, flags='C_CONTIGUOUS'), 	#Cy
np.ctypeslib.ndpointer(dtype=np.float64, ndim=2, flags='C_CONTIGUOUS'), 	#Dx
np.ctypeslib.ndpointer(dtype=np.float64, ndim=2, flags='C_CONTIGUOUS'), 	#Dy
np.ctypeslib.ndpointer(dtype=np.float64, ndim=1, flags='C_CONTIGUOUS'), 	#global_xs
np.ctypeslib.ndpointer(dtype=np.float64, ndim=1, flags='C_CONTIGUOUS'), 	#global_ys
ctypes.c_ulong,															#num_pts
ctypes.c_double,															#step
ctypes.c_ulong,															#imshape_x
ctypes.c_ulong,															#imshape_y
ctypes.c_bool,															#ascent
ctypes.c_ulong,															#num_ts
]

c_lib.euler_heun.argtypes = [
np.ctypeslib.ndpointer(dtype=np.float64, ndim=2, flags='C_CONTIGUOUS'), 	#Ax
np.ctypeslib.ndpointer(dtype=np.float64, ndim=2, flags='C_CONTIGUOUS'), 	#Ay
np.ctypeslib.ndpointer(dtype=np.float64, ndim=2, flags='C_CONTIGUOUS'), 	#By
np.ctypeslib.ndpointer(dtype=np.float64, ndim=2, flags='C_CONTIGUOUS'), 	#By
np.ctypeslib.ndpointer(dtype=np.float64, ndim=2, flags='C_CONTIGUOUS'), 	#Cx
np.ctypeslib.ndpointer(dtype=np.float64, ndim=2, flags='C_CONTIGUOUS'), 	#Cy
np.ctypeslib.ndpointer(dtype=np.float64, ndim=2, flags='C_CONTIGUOUS'), 	#Dx
np.ctypeslib.ndpointer(dtype=np.float64, ndim=2, flags='C_CONTIGUOUS'), 	#Dy
np.ctypeslib.ndpointer(dtype=np.float64, ndim=1, flags='C_CONTIGUOUS'), 	#global_xs
np.ctypeslib.ndpointer(dtype=np.float64, ndim=1, flags='C_CONTIGUOUS'), 	#global_ys
ctypes.c_ulong,															#num_pts
ctypes.c_double,															#global_step
ctypes.c_ulong,															#imshape_x
ctypes.c_ulong,															#imshape_y
ctypes.c_bool,															#ascent
ctypes.c_double,															#tol
ctypes.c_ulong,															#num_ts
]

c_lib.EH_rasterize.argtypes = [
np.ctypeslib.ndpointer(dtype=np.float64, ndim=2, flags='C_CONTIGUOUS'), 	#Ax
np.ctypeslib.ndpointer(dtype=np.float64, ndim=2, flags='C_CONTIGUOUS'), 	#Ay
np.ctypeslib.ndpointer(dtype=np.float64, ndim=2, flags='C_CONTIGUOUS'), 	#By
np.ctypeslib.ndpointer(dtype=np.float64, ndim=2, flags='C_CONTIGUOUS'), 	#By
np.ctypeslib.ndpointer(dtype=np.float64, ndim=2, flags='C_CONTIGUOUS'), 	#Cx
np.ctypeslib.ndpointer(dtype=np.float64, ndim=2, flags='C_CONTIGUOUS'), 	#Cy
np.ctypeslib.ndpointer(dtype=np.float64, ndim=2, flags='C_CONTIGUOUS'), 	#Dx
np.ctypeslib.ndpointer(dtype=np.float64, ndim=2, flags='C_CONTIGUOUS'), 	#Dy
np.ctypeslib.ndpointer(dtype=np.float64, ndim=1, flags='C_CONTIGUOUS'), 	#global_xs
np.ctypeslib.ndpointer(dtype=np.float64, ndim=1, flags='C_CONTIGUOUS'), 	#global_ys
ctypes.c_ulong,															#num_pts
ctypes.c_double,															#global_step
ctypes.c_ulong,															#imshape_x
ctypes.c_ulong,															#imshape_y
ctypes.c_bool,															#ascent
ctypes.c_double,															#tol
np.ctypeslib.ndpointer(dtype=np.uint8, ndim=2, flags='C_CONTIGUOUS'), 	#ws_lines
ctypes.c_ulong,															#num_ts
]

def explicit_euler(A, B, C, D, lattice_x, lattice_y, step, imshape, ascent, ts):
	'''
	Explicit Euler integration method corresponding to the Butcher tableu
	0 | 0
	- - -
	  | 1
	'''

	Ax, Ay = A
	Bx, By = B
	Cx, Cy = C
	Dx, Dy = D
	c_lib.explicit_euler(Ax, Ay, Bx, By, Cx, Cy, Dx, Dy, lattice_x, lattice_y, lattice_x.shape[0], step, imshape[0], imshape[1], ascent, ts)
	return lattice_x, lattice_y
	
def euler_heun(A, B, C, D, lattice_x, lattice_y, step, imshape, ascent, tol, ts):
	'''
	Adaptive Euler-Heun integration method corresponding to the Butcher tableu
	0	| 
	1	| 	1
	1/2	|	1/4	1/4
	-	-	-	-
		|	1/6	1/6	1/4
		|	2/3	2/3	0
	
	'''
	Ax, Ay = A
	Bx, By = B
	Cx, Cy = C
	Dx, Dy = D
	c_lib.euler_heun(Ax, Ay, Bx, By, Cx, Cy, Dx, Dy, lattice_x, lattice_y, lattice_x.shape[0], step, imshape[0], imshape[1], ascent, tol, ts)
	return lattice_x, lattice_y
	
def EH_rasterize(A, B, C, D, lattice_x, lattice_y, step, imshape, ascent, tol, ws_lines, ts):
	'''
	A version of the above adaptive EH method that also markes all the pixels the trajectories pass through.
	This is used for extracting the basins of attraction for a force-field.
	'''
	Ax, Ay = A
	Bx, By = B
	Cx, Cy = C
	Dx, Dy = D
	c_lib.EH_rasterize(Ax, Ay, Bx, By, Cx, Cy, Dx, Dy, lattice_x, lattice_y, lattice_x.shape[0], step, imshape[0], imshape[1], ascent, tol, ws_lines, ts)
	return ws_lines