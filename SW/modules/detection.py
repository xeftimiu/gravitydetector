import numpy as np
from scipy import ndimage
from scipy import signal, fft as sp_fft
from scipy.fft._helper import _init_nd_shape_and_axes
import modules.euler_heun as EH


class Convolver:
	'''
	Class for handling large-scale convolutions.
	Specifically, this class precomputes the kernels for calculating the gravitational force-fields.
	It can be instantiated multiple times for multithreading, though this largely negates performance gains.
	'''
	def __init__(self, img_shape, kernel1, kernel2):
		in1 = np.empty(img_shape)
		in2 = kernel1
		
		self.s1 = img_shape
		self.s2 = kernel1.shape
		
		
		in1, in2, self.axes = _init_freq_conv_axes(in1, in2, 'valid', None, sorted_axes=False)
		
		
		self.shape = [max((self.s1[i], self.s2[i])) if i not in self.axes else self.s1[i] + self.s2[i] - 1 for i in range(kernel1.ndim)]
		self.fshape = [sp_fft.next_fast_len(self.shape[a], True) for a in self.axes]
		self.fft, self.ifft = sp_fft.rfftn, sp_fft.irfftn
		
		
		self.conv1 = self.fft(kernel1, self.fshape, axes=self.axes)
		self.conv2 = self.fft(kernel2, self.fshape, axes=self.axes)
		
		
	def apply(self, img):
		sp1 = self.fft(img, self.fshape, axes=self.axes)
		ret1 = self.ifft(sp1 * self.conv1, self.fshape, axes=self.axes)
		ret2 = self.ifft(sp1 * self.conv2, self.fshape, axes=self.axes)

		fslice = tuple([slice(sz) for sz in self.shape])
		ret1 = ret1[fslice]
		ret2 = ret2[fslice]
		
		ret1 = _apply_conv_mode(ret1, self.s1, self.s2, 'valid', self.axes)
		ret2 = _apply_conv_mode(ret2, self.s1, self.s2, 'valid', self.axes)
		
		return ret1, ret2
	
'''--- SKIMAGE FUNCTIONS ---'''
	
def _init_freq_conv_axes(in1, in2, mode, axes, sorted_axes=False):
	s1 = in1.shape
	s2 = in2.shape
	noaxes = axes is None

	_, axes = _init_nd_shape_and_axes(in1, shape=None, axes=axes)

	if not noaxes and not len(axes):
		raise ValueError("when provided, axes cannot be empty")

	# Axes of length 1 can rely on broadcasting rules for multipy,
	# no fft needed.
	axes = [a for a in axes if s1[a] != 1 and s2[a] != 1]

	if sorted_axes:
		axes.sort()

	if not all(s1[a] == s2[a] or s1[a] == 1 or s2[a] == 1
			   for a in range(in1.ndim) if a not in axes):
		raise ValueError("incompatible shapes for in1 and in2:"
						 " {} and {}".format(s1, s2))

	# Check that input sizes are compatible with 'valid' mode.
	if _inputs_swap_needed(mode, s1, s2, axes=axes):
		# Convolution is commutative; order doesn't have any effect on output.
		in1, in2 = in2, in1

	return in1, in2, axes
		
def _inputs_swap_needed(mode, shape1, shape2, axes=None):
	if mode != 'valid':
		return False

	if not shape1:
		return False

	if axes is None:
		axes = range(len(shape1))

	ok1 = all(shape1[i] >= shape2[i] for i in axes)
	ok2 = all(shape2[i] >= shape1[i] for i in axes)

	if not (ok1 or ok2):
		raise ValueError("For 'valid' mode, one must be at least "
						 "as large as the other in every dimension")

	return not ok1

def _apply_conv_mode(ret, s1, s2, mode, axes):
	if mode == "full":
		return ret.copy()
	elif mode == "same":
		return _centered(ret, s1).copy()
	elif mode == "valid":
		shape_valid = [ret.shape[a] if a not in axes else s1[a] - s2[a] + 1
					   for a in range(ret.ndim)]
		return _centered(ret, shape_valid).copy()
	else:
		raise ValueError("acceptable mode flags are 'valid',"
						 " 'same', or 'full'")
						 
def _centered(arr, newshape):
	# Return the center newshape portion of the array.
	newshape = np.asarray(newshape)
	currshape = np.array(arr.shape)
	startind = (currshape - newshape) // 2
	endind = startind + newshape
	myslice = [slice(startind[k], endind[k]) for k in range(len(endind))]
	return arr[tuple(myslice)]

'''--- CUSTOM FUNCTIONS ---'''

def non_lattice(im_size, size, step):
	'''
	Create a uniform lattice of points over the image.
	The name refers to how the function creates 9 points in a neighbourhood
	to contrast a hex-lattice method that creates 6 points instead. (Only present in the development version.)
	
	This function is rarely used anymore, since the basin extraction is more accurate.
	'''
	im_y, im_x = im_size
	num_x = int(np.ceil(im_x/step))-1
	num_y = int(np.ceil(im_y/step))-1
	cover_x = num_x*step
	cover_y = num_y*step
	start_x = int(round(im_x/2-cover_x/2))
	start_y = int(round(im_y/2-cover_y/2))
	xs, ys = [], []
	for y in range(num_y+1):
		for x in range(num_x+1):
			cx, cy = start_x + x*step, start_y + y*step
			if cx < -size or cx > im_x+size or cy < -size or cy > im_y+size:
				continue
			xs.append(cx)
			ys.append(cy)
	return np.array(xs, np.float32), np.array(ys, np.float32)
		
def explicit_euler(A, B, C, D, xs_global, ys_global, step, imshape, ascent=False):
	'''
	Explicit Euler integration method corresponding to the Butcher tableu:
	0 | 0
	- - -
	  | 1
	
	This method is no longer used, since the C++ implementation is faster.
	
	'''
	Ax, Ay = A
	Bx, By = B
	Cx, Cy = C
	Dx, Dy = D
	
	oob_mask = (0.0 <= xs_global) & (xs_global < imshape[0]-1.0) & (0.0 <= ys_global) & (ys_global < imshape[1]-1.0)
	xs = xs_global[oob_mask]
	ys = ys_global[oob_mask]
	
	left = np.floor(xs).astype(np.uint16)
	top = np.floor(ys).astype(np.uint16)
	xmod, ymod = xs-left, ys-top

	x_avg = Ax[left, top] + Bx[left, top]*ymod + Cx[left, top]*xmod + Dx[left, top]*xmod*ymod
	y_avg = Ay[left, top] + By[left, top]*ymod + Cy[left, top]*xmod + Dy[left, top]*xmod*ymod
	
	xs = xs + y_avg*step*(1-2*int(ascent))
	ys = ys + x_avg*step*(1-2*int(ascent))
	
	xs_global[oob_mask] = xs
	ys_global[oob_mask] = ys
		
class GravityDetector:
	'''
	Class for handling gradient descent/ascent.
	It mostly exists as a wrapper for the C++ functions, though some calculations are performed using Numpy.
	'''
	def __init__(self, img_shape, props):
		distance_mult = props['distance_mult']
		receptive_field, margin = 38, 10
		self.lattice_x, self.lattice_y = non_lattice((img_shape[1], img_shape[0]), receptive_field, margin)
		if props.get('conv_shape', '') == 'dynamic':
			X, Y = np.meshgrid(np.arange(-img_shape[0], img_shape[0]+1)*distance_mult, np.arange(-img_shape[0], img_shape[0]+1)*distance_mult)
		else:
			csize = props.get('conv_size', 256)
			X, Y = np.meshgrid(np.arange(-csize, csize+1)*distance_mult, np.arange(-csize, csize+1)*distance_mult)
		distance = np.sqrt(X**2 + Y**2 + 1) # glass plate over objects +1
		unit_x, unit_y = X/(distance**3), Y/(distance**3)
		if props.get('conv_shape', '') == 'dynamic':
			self.convolver = Convolver((img_shape[0]*3, img_shape[1]*3), unit_x, unit_y)
		else:
			csize = props.get('conv_size', 256)
			self.convolver = Convolver((img_shape[0]+(2*csize), img_shape[1]+(2*csize)), unit_x, unit_y)
		self.step = props['step_size']
		
	def gradient_descent(self, img, props):
		'''
		Perform a dense descent to approximate the basins of attraction.
		This method is faster than the exact extraction, but much less accurate.
		'''
		G = props['gravity_mult']
		
		if props.get('conv_shape', '') == 'dynamic':
			padded_img = np.pad(img, ((img.shape[0], img.shape[0]), (img.shape[1], img.shape[1])), mode='reflect')
		else:
			csize = props.get('conv_size', 256)
			padding = csize-1
			padded_img = np.pad(img, ((padding, padding), (padding, padding)), mode='reflect')
		field_x, field_y = self.convolver.apply(padded_img)
		field_x, field_y = -G*field_x, -G*field_y
		Ax, Ay = field_x[:-1, :-1], field_y[:-1, :-1]
		Bx, By = field_x[:-1, 1:] - field_x[:-1, :-1], field_y[:-1, 1:] - field_y[:-1, :-1]
		Cx, Cy = field_x[1:, :-1] - field_x[:-1, :-1], field_y[1:, :-1] - field_y[:-1, :-1]
		Dx, Dy = field_x[1:, 1:] + field_x[:-1, :-1] - field_x[1:, :-1] - field_x[:-1, 1:], field_y[1:, 1:] + field_y[:-1, :-1] - field_y[1:, :-1] - field_y[:-1, 1:]
		
		lattice_x, lattice_y = self.lattice_x.copy(), self.lattice_y.copy()
		iteration = 0
		
		for iteration in range(props.get('descent_iterations', 400)+1):
			explicit_euler((Ax, Ay), (Bx, By), (Cx, Cy), (Dx, Dy), lattice_x, lattice_y, self.step, img.shape)
		
		flat_nearest = np.ravel_multi_index((np.clip(np.round(lattice_x).astype(np.uint16), 0, img.shape[0]-1), np.clip(np.round(lattice_y).astype(np.uint16), 0, img.shape[1]-1)), img.shape)
		nls, nl_counts = np.unique(flat_nearest, return_counts=True)
		final_xs, final_ys = [], []
		labels = np.zeros_like(img, np.int16)
		l = 1
		for nl, c in zip(nls, nl_counts):
			idx_x, idx_y = np.unravel_index(nl, img.shape)
			if c < props['merge_filter']:
				labels[idx_x, idx_y] = -1
				continue
			else:		
				final_xs.append(idx_x)
				final_ys.append(idx_y)
				labels[idx_x, idx_y] = l
				l += 1
		
		if props.get('analysis', False):
			plt.imshow(cv2.morphologyEx((labels>0).astype(np.uint8), cv2.MORPH_DILATE, np.ones((7, 7))), alpha=0.5)
			plt.imshow(img, cmap='gray', alpha=0.5)
			plt.show()

		return final_xs, final_ys, labels
		
	def basin_extraction(self, img, props):
		'''
		This function extract the basins of attraction with subpixel precision.
		The outline of the algorithm is as follows:
		
		1. Calculate the force-fields
		2. Locate the critical points of the bilinear interpolation by linearizing the system of PDEs using the Jacobian matrix
		3. Classify the equilibria into sinks/sources/saddles using the trace-determinant/Poincaré method
		4. Perform gradient ascent from the saddle points until all minima are separated from each other
		5. Perform descent from every basin to locate the corresponding minimum
		'''
		G = props['gravity_mult']
		
		if props.get('conv_shape', '') == 'dynamic':
			padded_img = np.pad(img, ((img.shape[0], img.shape[0]), (img.shape[1], img.shape[1])), mode='reflect')
		else:
			csize = props.get('conv_size', 256)
			padding = csize-1
			padded_img = np.pad(img, ((padding, padding), (padding, padding)), mode='reflect')
		
		# Field calculation
		padded_img = padded_img.astype(np.float64)
		field_x, field_y = self.convolver.apply(padded_img)
		field_x, field_y = -G*field_x, -G*field_y
		
		# Bilinear interpolation
		Ax, Ay = field_x[:-1, :-1], field_y[:-1, :-1]
		Bx, By = field_x[:-1, 1:] - field_x[:-1, :-1], field_y[:-1, 1:] - field_y[:-1, :-1]
		Cx, Cy = field_x[1:, :-1] - field_x[:-1, :-1], field_y[1:, :-1] - field_y[:-1, :-1]
		Dx, Dy = field_x[1:, 1:] + field_x[:-1, :-1] - field_x[1:, :-1] - field_x[:-1, 1:], field_y[1:, 1:] + field_y[:-1, :-1] - field_y[1:, :-1] - field_y[:-1, 1:]
		Ax = np.ascontiguousarray(Ax)
		Ay = np.ascontiguousarray(Ay)
		Bx = np.ascontiguousarray(Bx)
		By = np.ascontiguousarray(By)
		Cx = np.ascontiguousarray(Cx)
		Cy = np.ascontiguousarray(Cy)
		Dx = np.ascontiguousarray(Dx)
		Dy = np.ascontiguousarray(Dy)
		
		
		lefts, tops = np.meshgrid(np.arange(0, img.shape[0]-1), np.arange(0, img.shape[1]-1), indexing='ij')
		
		# Critical points
		deg2 = Bx*Dy - By*Dx
		deg1 = Ax*Dy + Bx*Cy - Ay*Dx - By*Cx
		deg0 = Ax*Cy - Ay*Cx
		det = deg1**2 - 4*deg2*deg0
		sol_mask = det >= 0
		sol_mask_nz = np.nonzero(sol_mask)
		sol1 = (-deg1[sol_mask] + np.sqrt(det[sol_mask]))/(2*deg2[sol_mask])
		sol2 = (-deg1[sol_mask] - np.sqrt(det[sol_mask]))/(2*deg2[sol_mask])
		
		first_mask = (0 < sol1) & (sol1 <= 1)
		second_mask = (0 < sol2) & (sol2 <= 1)
		nz1 = (Cy[sol_mask][first_mask]+Dy[sol_mask][first_mask]*sol1[first_mask]) != 0
		nz2 = (Cy[sol_mask][second_mask]+Dy[sol_mask][second_mask]*sol2[second_mask]) != 0
		
		soly1 = -(Ay[sol_mask][first_mask][nz1]+By[sol_mask][first_mask][nz1]*sol1[first_mask][nz1]) / (Cy[sol_mask][first_mask][nz1]+Dy[sol_mask][first_mask][nz1]*sol1[first_mask][nz1])
		soly2 = -(Ay[sol_mask][second_mask][nz2]+By[sol_mask][second_mask][nz2]*sol2[second_mask][nz2]) / (Cy[sol_mask][second_mask][nz2]+Dy[sol_mask][second_mask][nz2]*sol2[second_mask][nz2])
		
		ymask1 = np.logical_and(0 <= soly1, soly1 <= 1)
		ymask2 = np.logical_and(0 <= soly2, soly2 <= 1)
		
		first_nz, second_nz = np.count_nonzero(ymask1), np.count_nonzero(ymask2)
		solsx, solsy = np.zeros(first_nz + second_nz), np.zeros(first_nz + second_nz)
		solsx[:first_nz], solsy[:first_nz] = sol1[first_mask][nz1][ymask1], soly1[ymask1]
		solsx[first_nz:], solsy[first_nz:] = sol2[second_mask][nz2][ymask2], soly2[ymask2]
		
		full_mask1 = np.ravel_multi_index((lefts[sol_mask][first_mask][nz1][ymask1], tops[sol_mask][first_mask][nz1][ymask1]), Ax.shape, order='C')
		full_mask2 = np.ravel_multi_index((lefts[sol_mask][second_mask][nz2][ymask2], tops[sol_mask][second_mask][nz2][ymask2]), Ax.shape, order='C')
		flat_mask = np.append(full_mask1, full_mask2)

		# Classify equilibria
		zero_xs, zero_ys = solsx, solsy
		zero_pxx, zero_pxy = lefts.flat[flat_mask] + (zero_ys > 0.5).astype(np.uint8), tops.flat[flat_mask] + (zero_xs > 0.5).astype(np.uint8)
	
		Jxx, Jxy = Bx.flat[flat_mask] + Dx.flat[flat_mask]*zero_ys, Cx.flat[flat_mask] + Dx.flat[flat_mask]*zero_xs
		Jyx, Jyy = By.flat[flat_mask] + Dy.flat[flat_mask]*zero_ys, Cy.flat[flat_mask] + Dy.flat[flat_mask]*zero_xs
		
		Jdet = Jxx*Jyy-Jxy*Jyx
		JT = Jxx + Jyy
		
		deg2 = 1
		deg1 = -(Jxx + Jyy)
		deg0 = Jxx*Jyy - Jxy*Jyx

		det = np.sqrt((deg1**2 - 4*deg2*deg0).astype(np.cdouble))
		eig1 = (-deg1 + det)/(2*deg2)
		eig2 = (-deg1 - det)/(2*deg2)
		
		v1x, v1y = np.ones_like(zero_pxx), -(Jxx-eig1.real)/Jxy
		v2x, v2y = np.ones_like(zero_pxx), -(Jxx-eig2.real)/Jxy
		mag1, mag2 = np.sqrt(v1x**2 + v1y**2), np.sqrt(v2x**2 + v2y**2)
		v1x, v1y = v1x/mag1, v1y/mag1
		v2x, v2y = v2x/mag2, v2y/mag2
		eigs = np.zeros((v1x.shape[0], 2)) 
		eigvecs = np.zeros((v1x.shape[0], 2, 2)) # v, 1/2, x/y
		eigs[:, 0], eigs[:, 1] = eig1.real, eig2.real
		eigvecs[:, 0, 0], eigvecs[:, 0, 1] = v1x, v1y
		eigvecs[:, 1, 0], eigvecs[:, 1, 1] = v2x, v2y
		
		sources = (Jdet > 0) & (JT > 0)
		sinks = (Jdet > 0) & (JT < 0)
		saddles = Jdet < 0
		
		equilibria = np.zeros_like(zero_pxx)
		
		equilibria[sources] = 1 # Source (simple or spiral)
		equilibria[sinks] = 2 # Sink (simple or spiral)
		equilibria[saddles] = 3 # Saddle
		
		eq_img = np.zeros_like(img)
		sink_img = np.zeros_like(img)
		sink_img2 = np.zeros_like(img, np.uint16)
		eq_img[zero_pxx, zero_pxy] = equilibria
		sink_img[zero_pxx[sinks], zero_pxy[sinks]] = 1
		sink_img2[zero_pxx[sinks], zero_pxy[sinks]] = np.arange(1, np.count_nonzero(sinks)+1)
		
		# Set up ascent
		ascent_x = lefts.flat[flat_mask][saddles] + zero_ys[saddles]
		ascent_y = tops.flat[flat_mask][saddles] + zero_xs[saddles]
		saddle_eigs = eigs[saddles]
		saddle_vecs = eigvecs[saddles]
		saddle_stable_vecs = np.zeros((saddle_eigs.shape[0], 2))
		saddle_stable_vecs[saddle_eigs[:, 0] < 0] = saddle_vecs[saddle_eigs[:, 0] < 0, 0]
		saddle_stable_vecs[saddle_eigs[:, 1] < 0] = saddle_vecs[saddle_eigs[:, 1] < 0, 1]
		ascent_x = np.clip(np.concatenate((ascent_x - saddle_stable_vecs[:, 1]*self.step, ascent_x + saddle_stable_vecs[:, 1]*self.step)), 0.5, img.shape[0]-1.5)
		ascent_y = np.clip(np.concatenate((ascent_y - saddle_stable_vecs[:, 0]*self.step, ascent_y + saddle_stable_vecs[:, 0]*self.step)), 0.5, img.shape[1]-1.5)
		
		ws_lines = np.zeros_like(img, np.uint8)
		ws_lines[0, :], ws_lines[-1, :] = 1, 1
		ws_lines[:, 0], ws_lines[:, -1] = 1, 1
		ws_lines = np.ascontiguousarray(ws_lines)
		steps_global = np.ones_like(ascent_x)*self.step
		steps = steps_global.copy()
		tol = 0.01
		ascent_x = np.ascontiguousarray(ascent_x.astype(np.float64))
		ascent_y = np.ascontiguousarray(ascent_y.astype(np.float64))
		
		last = False
		#Adaptive Euler-Heun
		'''for i in range(10):
			EH.EH_rasterize((Ax, Ay), (Bx, By), (Cx, Cy), (Dx, Dy), ascent_x, ascent_y, self.step, img.shape, True, 0.01, ws_lines, 500)
			dense_labels, num_labels = ndimage.label(1-ws_lines, np.ones((3, 3)))
			num_sinks = ndimage.sum_labels(sink_img, dense_labels, index=np.arange(1, num_labels+1))
			if last:
				break
			if np.max(num_sinks) < 2:
				last = True'''
		EH.EH_rasterize((Ax, Ay), (Bx, By), (Cx, Cy), (Dx, Dy), ascent_x, ascent_y, self.step, img.shape, True, 0.01, ws_lines, 5000)
		dense_labels, _ = ndimage.label(1-ws_lines, np.ones((3, 3)))
		
		dense_labels_og = dense_labels.copy()
		unique_labels = np.unique(dense_labels[dense_labels != 0])
		
		dist_trans = ndimage.distance_transform_edt(dense_labels)
		dt_maxes = ndimage.maximum_position(dist_trans, dense_labels, unique_labels)
		lattice_x, lattice_y = np.array([dtm[0] for dtm in dt_maxes], np.float32), np.array([dtm[1] for dtm in dt_maxes], np.float32)
		lattice_x = np.ascontiguousarray(lattice_x.astype(np.float64))
		lattice_y = np.ascontiguousarray(lattice_y.astype(np.float64))
		for i in range(10):
			lattice_x, lattice_y = EH.euler_heun((Ax, Ay), (Bx, By), (Cx, Cy), (Dx, Dy), lattice_x, lattice_y, self.step, img.shape, False, 0.01, 500)
			ex, ey = np.floor(np.clip(lattice_x, -0.499999, img.shape[0]-1.50001)+0.5).astype(np.uint16), np.floor(np.clip(lattice_y, -0.499999, img.shape[1]-1.50001)+0.5).astype(np.uint16)
			if np.all(sink_img2[ex, ey] != 0):
				break
		
		ex, ey = np.floor(np.clip(lattice_x, -0.499999, img.shape[0]-1.50001)+0.5).astype(np.uint16), np.floor(np.clip(lattice_y, -0.499999, img.shape[1]-1.50001)+0.5).astype(np.uint16)
		label_idxs = sink_img2[ex, ey]
		label_basin_map = {k: v for k, v in zip(unique_labels, label_idxs)}
		temp_map = np.zeros(np.max(dense_labels)+1, np.uint16)
		for k, v in label_basin_map.items():
			temp_map[k] = v
		dense_labels = temp_map[dense_labels]
		return dense_labels, sink_img2
		