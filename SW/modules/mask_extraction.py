import pathlib
import ctypes
import cv2
from scipy import ndimage
import numpy as np
from skimage.morphology import reconstruction
from itertools import cycle
import sys
import heapq
import matplotlib.pyplot as plt

if sys.platform == 'win32':
	libname = pathlib.Path('./modules/mask_extraction.dll').absolute()
elif sys.platform == 'linux':
	libname = pathlib.Path('./modules/mask_extraction.so').absolute()
else:
	print(f'OS type {sys.platform} not supported!')
	exit(1)
c_lib = ctypes.CDLL(str(libname.resolve()), winmode=0)

c_lib.extract_mask.argtypes = [
np.ctypeslib.ndpointer(dtype=np.uint8, ndim=2, flags='C_CONTIGUOUS'), 	#img
np.ctypeslib.ndpointer(dtype=np.int16, ndim=2, flags='C_CONTIGUOUS'), 	#set
np.ctypeslib.ndpointer(dtype=np.uint8, ndim=2, flags='C_CONTIGUOUS'), 	#walls
np.ctypeslib.ndpointer(dtype=np.uint64, ndim=1, flags='C_CONTIGUOUS'),	#locs
np.ctypeslib.ndpointer(dtype=np.uint64, ndim=3, flags='C_CONTIGUOUS'),	#neighs
ctypes.c_ulong,															#num_pts
ctypes.c_char,															#num_neighs
ctypes.c_float,															#wall_thresh
ctypes.c_float															#halve_thresh
]

''' --- SKIMAGE FUNCTIONS --- '''

class _fcycle(object):

    def __init__(self, iterable):
        """Call functions from the iterable each time it is called."""
        self.funcs = cycle(iterable)

    def __call__(self, *args, **kwargs):
        f = next(self.funcs)
        return f(*args, **kwargs)


# SI and IS operators for 2D and 3D.
_P2 = [np.eye(3).astype(np.uint8),
       np.array([[0, 1, 0]] * 3).astype(np.uint8),
       np.flipud(np.eye(3)).astype(np.uint8),
       np.rot90([[0, 1, 0]] * 3).astype(np.uint8)]
_P3 = [np.zeros((3, 3, 3)) for i in range(9)]

_P3[0][:, :, 1] = 1
_P3[1][:, 1, :] = 1
_P3[2][1, :, :] = 1
_P3[3][:, [0, 1, 2], [0, 1, 2]] = 1
_P3[4][:, [0, 1, 2], [2, 1, 0]] = 1
_P3[5][[0, 1, 2], :, [0, 1, 2]] = 1
_P3[6][[0, 1, 2], :, [2, 1, 0]] = 1
_P3[7][[0, 1, 2], [0, 1, 2], :] = 1
_P3[8][[0, 1, 2], [2, 1, 0], :] = 1


def sup_inf(u):
    """SI operator."""

    if np.ndim(u) == 2:
        P = _P2
    elif np.ndim(u) == 3:
        P = _P3
    else:
        raise ValueError("u has an invalid number of dimensions "
                         "(should be 2 or 3)")

    erosions = []
    for P_i in P:
        erosions.append(cv2.morphologyEx(u, cv2.MORPH_ERODE, P_i))
        #erosions.append(ndimage.grey_erosion(u, footprint=P_i))

    return np.array(erosions, dtype=np.uint16).max(0)


def inf_sup(u):
    """IS operator."""

    if np.ndim(u) == 2:
        P = _P2
    elif np.ndim(u) == 3:
        P = _P3
    else:
        raise ValueError("u has an invalid number of dimensions "
                         "(should be 2 or 3)")

    dilations = []
    for P_i in P:
        dilations.append(cv2.morphologyEx(u, cv2.MORPH_DILATE, P_i))
        #dilations.append(ndimage.grey_dilation(u, footprint=P_i))

    return np.array(dilations, dtype=np.uint16).min(0)


_curvop = _fcycle([lambda u: sup_inf(inf_sup(u)),  # SIoIS
                   lambda u: inf_sup(sup_inf(u))])  # ISoSI

''' --- CUSTOM FUNCTIONS --- '''

def extract_masks_morphology(img, props, init_set=None, xs=None, ys=None):
	'''
	Extract the cell masks using repeated dilations and smoothings.
	This version is outdated, since it is unable to handle multiple masks concurrently.
	As a result, touching masks will begin overwriting each other, which can give inconsistent results. 
	'''
	wall_thresh = props['wall_thresh_mult']
	iterations = props['mask_iterations']
	smoothing = props['smoothing']
	smoothing_start = props['smoothing_start']
	
	if init_set is None:
		init_set = np.zeros_like(img, np.uint16)
		for idx, (x, y) in enumerate(zip(xs, ys)):
			cv2.circle(init_set, (int(y), int(x)), 1, idx+1, cv2.FILLED)
		num_labels = len(xs)+1
	else:
		#us = np.unique(init_set)
		num_labels = np.max(init_set)+1
	#---EXTRACTION---
	set = init_set
	#prev = init_set.copy()
	walls = np.zeros_like(img)
	for iter in range(iterations):
		print(f'iter: {iter}')
		uniques = np.unique(set)
		means = np.zeros(num_labels)
		means[uniques] = ndimage.mean(img, set, uniques)
		means[0] = 0
		
		arg_idxs = np.argsort(means[1:])					#Need to sort to prevent lower index labels overtaking higher ones
		arg_idxs2 = np.pad(np.argsort(arg_idxs)+1, (1, 0))	#0 index must stay in the first position
		set = (arg_idxs2[set]).astype(np.uint16)
		means = means[np.pad(arg_idxs+1, (1, 0))]
		mosaic = means[set]
		
		kernel = np.ones((7, 7), np.uint8)
		kernel[3, 3] = 0
		neighs = cv2.morphologyEx(mosaic, cv2.MORPH_DILATE, kernel)
		walls = np.logical_or(walls, neighs*(1-neighs*wall_thresh) > img)
		set = cv2.morphologyEx(set, cv2.MORPH_DILATE, np.ones((3, 3)))
		if iter > smoothing_start:
			for i in range(smoothing):
				set = _curvop(set)
		set[walls > 0] = 0

	
	#---EXTRACTION---

	conts, _ = cv2.findContours((set > 0).astype(np.uint8), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
	cells = np.zeros_like(img, np.uint8)
	cv2.drawContours(cells, conts, -1, 255, cv2.FILLED)
	
	num_labels, labels, stats, centroids = cv2.connectedComponentsWithStats(cells, 8)
	
	boundary = props['boundary']
	delete = [i for i in range(num_labels) if stats[i][cv2.CC_STAT_AREA] > props['size_filter_upper'] or stats[i][cv2.CC_STAT_AREA] < props['size_filter_lower'] or
	(stats[i][cv2.CC_STAT_LEFT] > img.shape[1]-boundary or stats[i][cv2.CC_STAT_TOP] + stats[i][cv2.CC_STAT_HEIGHT] < boundary or
	stats[i][cv2.CC_STAT_LEFT] + stats[i][cv2.CC_STAT_WIDTH] < boundary or stats[i][cv2.CC_STAT_TOP] > img.shape[0]-boundary)]
	mask0 = labels == 0
	mask1 = np.isin(labels, delete)
	labels[mask0] = 0
	labels[mask1] = 0
	
	conts, _ = cv2.findContours((labels > 0).astype(np.uint8), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
	cells = np.zeros_like(img)
	cv2.drawContours(cells, conts, -1, 255, cv2.FILLED)
	num_labels, labels, stats, centroids = cv2.connectedComponentsWithStats(cells, 8)
	
	
	return labels
	
def extract_masks_heap(img, xs, ys, wall_thresh=0.0033):
	'''
	Extract the cell masks using a max-heap approach.
	Seed pixels are inserted into the heap, then in the order of highest to lowest value
	they are examined one-by-one. If local contrast exceeds a given threshold, the pixel is treated as a wall.
	Otherwise its neighbours are added to the heap and the process repeats until termination.
	This function is almost identical to the C++ version, and is therefore deprecated.
	'''
	img = img.astype(np.int16)
	padded_img = np.pad(img, ((1, 1), (1, 1)))
	xs, ys = np.array(xs), np.array(ys)
	heap = []
	values = [-img[x, y] for x, y in zip(xs, ys)]
	v_sort = np.argsort(values)
	set = np.zeros_like(padded_img, np.uint16)
	walls = np.zeros_like(padded_img, np.uint8)
	walls[0, :] = 1
	walls[-1, :] = 1
	walls[:, 0] = 1
	walls[:, -1] = 1
	for idx, (x, y) in enumerate(zip(xs[v_sort], ys[v_sort])):
		heap.append((idx+1, -img[x, y], (x+1, y+1)))
	
	Xs, Ys = np.meshgrid(np.arange(img.shape[0]), np.arange(img.shape[1]))
	Xs += 1
	Ys += 1
	neighbours = np.zeros((padded_img.shape[0], padded_img.shape[1], 8), np.uint64)
	idx = 0
	for i in range(-1, 2):
		for j in range(-1, 2):
			if i == 0 and j == 0:
				continue
			neighbours[1:-1, 1:-1, idx] = np.ravel_multi_index((Ys+j, Xs+i), padded_img.shape)
			idx += 1
	
	
	#---EXTRACTION---

	curr_idx = 1
	curr_sum = 0
	curr_count = 0
	iter = 0
	while len(heap) > 0:
		idx, value, (x, y) = heapq.heappop(heap)
		if set[x, y] != 0 or walls[x, y] != 0:
			continue
		if idx != curr_idx:
			plt.imshow(set)
			plt.show()
			curr_idx = idx
			curr_sum = 0
			curr_count = 0
		set[x, y] = idx
		curr_sum += padded_img[x, y]
		curr_count += 1
		avg = curr_sum/curr_count
		if avg*(1-avg*wall_thresh) > padded_img[x, y]:
			walls[x, y] = 1
			set[x, y] = 0
		else:
			neighs = neighbours[x, y]
			zs = neighs[np.logical_and(set.flat[neighs] == 0, walls.flat[neighs] == 0)]
			nidxs = np.unravel_index(zs, padded_img.shape)
			for nx, ny in zip(nidxs[0], nidxs[1]):
				heapq.heappush(heap, (idx, -padded_img[nx, ny], (nx, ny)))
			iter += 1

	set = set[1:-1, 1:-1]
	conts, _ = cv2.findContours((set > 0).astype(np.uint8), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
	cells = np.zeros_like(img, np.uint8)
	cv2.drawContours(cells, conts, -1, 255, cv2.FILLED)
	
	#num_labels, labels, stats, centroids = cv2.connectedComponentsWithStats(cells, 8)
	labels, num_labels = ndimage.label(cells, np.ones((3, 3)))	
	
	#---EXTRACTION---
	
	return labels
	
def extract_masks_cpp(img, xs, ys, props):
	'''
	A wrapper around the C++ implementation of the above heap-based function.
	'''
	padded_img = np.pad(img, ((1, 1), (1, 1)))
	xs, ys = np.array(xs), np.array(ys)
	set = np.zeros_like(padded_img, np.int16)
	walls = np.zeros_like(padded_img, np.uint8)
	walls[0, :] = 1
	walls[-1, :] = 1
	walls[:, 0] = 1
	walls[:, -1] = 1
	
	values = [-float(img[x, y]) for x, y in zip(xs, ys)]
	v_sort = np.argsort(values)
	
	locs = np.ravel_multi_index((xs[v_sort]+1, ys[v_sort]+1), padded_img.shape).astype(np.uint64)
	Xs, Ys = np.meshgrid(np.arange(img.shape[0]), np.arange(img.shape[1]))
	Xs += 1
	Ys += 1
	neighbours = np.zeros((padded_img.shape[0], padded_img.shape[1], 8), np.uint64)
	idx = 0
	for i in range(-1, 2):
		for j in range(-1, 2):
			if i == 0 and j == 0:
				continue
			neighbours[1:-1, 1:-1, idx] = np.ravel_multi_index((Ys+j, Xs+i), padded_img.shape)
			idx += 1

	c_lib.extract_mask(padded_img, set, walls, locs, neighbours, xs.shape[0], 8, props['wall_thresh_mult'], props['wall_thresh_halve'])

	set = set[1:-1, 1:-1]
	conts, _ = cv2.findContours((set > 0).astype(np.uint8), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
	cells = np.zeros_like(img)
	cv2.drawContours(cells, conts, -1, 255, cv2.FILLED)
	
	
	
	num_labels, labels, stats, centroids = cv2.connectedComponentsWithStats(cells, 8)
	return labels
	
def extract_masks(img, props, init_set=None, xs=None, ys=None):
	'''
	A dispatcher function for cell mask extraction.
	'''
	if props['mask_method'] == 'morphology':
		try:
			return extract_masks_morphology(img, props, init_set, xs, ys)
		except KeyError:
			print('Error in mask extraction')
	elif props['mask_method'] == 'heap':
		return extract_masks_heap(img, xs, ys, props['wall_thresh_mult'])
	elif props['mask_method'] == 'cpp':
		return extract_masks_cpp(img, xs, ys, props)
	else:
		print(f'Unknown mask extraction method: {props["mask_method"]}')
		return None