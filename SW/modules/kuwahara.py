import pycuda.autoinit
from pycuda.autoinit import context
import pycuda.driver as cuda
from pycuda.compiler import SourceModule

import numpy as np
import os

BLOCK_THREADS = 512

kernel_code = ''
with open('./modules/kuwahara_kernel_code.cpp', 'r') as file:
	kernel_code = file.read()
mod = SourceModule(kernel_code)
structure_tensor_2D = mod.get_function('structure_tensor_2D')
convolve_2D = mod.get_function('convolve_2D')
run_kuwahara_2D = mod.get_function('run_kuwahara_2D')


def _get_gaussian_kernel(sigma):
	'''
	Create a 0-mean, [sigma]-variance gaussian kernel.
	The width is equal to 4-sigma.
	'''
	fwhm = 2*np.sqrt(2*np.log(2))*sigma;
	gauss_base = -4*np.log(2)/(fwhm**2);
	limit = int(4.0*sigma + 0.5);
	ksize = 2*limit + 1;
	output = np.zeros(ksize)
	for i in range(-limit, limit+1):
		output[i+limit] = np.exp(gauss_base*(i**2))
	output /= np.sum(output)
	
	return limit, output;
	
def Kuwahara_single(img, N=8, r=6, sigma=0.4, alpha=1.0, q=4):
	'''
	Initialize the CUDA context and perform Kuwahara filtering using the C++/CUDA implementation.
	The buffers could be reused between runs, but this version is meant for single images only.
	A more efficient implementation is in the development version.
	'''
	ret_gray = False
	if len(img.shape) < 3:
		img = np.repeat(img[:, :, np.newaxis], 3, axis=2)
		ret_gray = True
	w, h, c = img.shape
	w, h = np.int32(w), np.int32(h)
	nPixels = int(w*h)
	
	result_gpu = cuda.mem_alloc(nPixels * 3 * np.dtype(np.uint8).itemsize)
	cuda.memset_d8(result_gpu, 0, nPixels * 3 * np.dtype(np.uint8).itemsize)
	structure_gpu = cuda.mem_alloc(nPixels * 3 * np.dtype(np.float32).itemsize)
	cuda.memset_d8(structure_gpu, 0, nPixels * 3 * np.dtype(np.float32).itemsize)
	smoothed_gpu = cuda.mem_alloc(nPixels * 3 * np.dtype(np.float32).itemsize)
	cuda.memset_d8(smoothed_gpu, 0, nPixels * 3 * np.dtype(np.float32).itemsize)
	
	ksize, kernel = _get_gaussian_kernel(1.6)
	ksize = np.int32(ksize)
	kernel_gpu = cuda.to_device(kernel.flatten('C'))
	
	#input_gpu = cuda.mem_alloc(nPixels*np.dtype(np.uint8).itemsize)
	#cuda.memcpy_htod(input_gpu, img.flatten('C'))
	input_gpu = cuda.to_device(img.flatten('C'))
	
	structure_tensor_2D(input_gpu, structure_gpu, w, h, block=(BLOCK_THREADS, 1, 1), grid=(int(nPixels / BLOCK_THREADS), 1, 1))
	context.synchronize()
	convolve_2D(structure_gpu, smoothed_gpu, kernel_gpu, ksize, w, h, np.int32(1), np.int32(0), block=(BLOCK_THREADS, 1, 1), grid=(int(nPixels / BLOCK_THREADS), 1, 1))
	context.synchronize()
	convolve_2D(smoothed_gpu, structure_gpu, kernel_gpu, ksize, w, h, np.int32(0), np.int32(1), block=(BLOCK_THREADS, 1, 1), grid=(int(nPixels / BLOCK_THREADS), 1, 1))
	context.synchronize()
	
	fwhm = 2*np.sqrt(2*np.log(2))*sigma
	gauss_base = np.float32(-4*np.log(2)/(fwhm**2))
	xi = np.float32(2/r)
	gamma = 3*np.pi/(2*N)
	sg = np.sin(gamma)
	eta = np.float32((xi + np.cos(gamma))/(sg**2))
	
	run_kuwahara_2D(input_gpu, result_gpu, w, h, structure_gpu, np.int32(N), np.float32(r), gauss_base, xi, eta, np.float32(alpha), np.int32(q), block=(BLOCK_THREADS, 1, 1), grid=(int(nPixels / BLOCK_THREADS), 1, 1))
	context.synchronize()
	
	result = cuda.from_device(result_gpu, (w, h, 3), np.uint8)
	if ret_gray:
		result = (0.299*result[:, :, 0] + 0.587*result[:, :, 1] + 0.114*result[:, :, 2]).astype(np.uint8)
	return result