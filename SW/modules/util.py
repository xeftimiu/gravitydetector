import numpy as np
import cv2
from skimage import morphology
from scipy import ndimage
import modules.chan_vese as CV

def fill_holes(img, method='erosion'):
	'''
	Use morphological reconstruction to fix any holes in the interiors of cell masks.
	This function can only handle fully enclosed regions, not convexity defects.
	'''
	marker = np.copy(img)
	if method == 'erosion':
		marker[1:-1, 1:-1] = img.max()
	else:
		marker[1:-1, 1:-1] = img.min()
	r = morphology.reconstruction(marker, img, method=method)
	return r
	
def circularity(labels):
	'''
	Calculate circularity of masks.
	This implementation is based on the work of Bottema (see: 10.1109/ICASSP.2000.859286)
	since the perimeter^2/(4*pi*area) method is really inaccurate for small masks
	'''
	labels2 = labels.copy()
	locs = ndimage.value_indices(labels)
	for lu, (lxs, lys) in locs.items():
		if lu == 0:
			continue
		lc = lxs.shape[0]
		mx = np.mean(lxs)
		my = np.mean(lys)
		r = np.sqrt(lc/np.pi)
		cv2.circle(labels2, (int(my), int(mx)), int(np.round(r)), 0, -1)
	locs2 = ndimage.value_indices(labels2)
	circos = {}
	for lu, (lxs, lys) in locs.items():
		if lu == 0:
			continue
		try:
			lxs2, lys2 = locs2[lu]
			circ = lxs2.shape[0]/lxs.shape[0]
			circos[lu] = circ
		except KeyError:
			circos[lu] = 0
	
	return circos
	
def CV_refine(props, img, mask, label):
	'''
	Refine existing labels with probabilistic Chan-Vese.
	'''
	xs, ys = np.nonzero(mask==label)
	bb = (np.min(xs), np.max(xs), np.min(ys), np.max(ys))
	w, h = bb[1]-bb[0], bb[3]-bb[2]
	cx, cy = bb[0]+w//2, bb[2]+h//2
	xoff, yoff = int(1.5*w/2),  int(1.5*h/2)
	wstartx, wendx = max(0, cx-xoff), min(img.shape[0], cx+xoff)
	wstarty, wendy = max(0, cy-yoff), min(img.shape[1], cy+yoff)
	if wstartx == wendx or wstarty == wendy:
		return False
	curr_window = img[wstartx:wendx, wstarty:wendy]
	curr_window_mask = mask[wstartx:wendx, wstarty:wendy]
	init_mask = (curr_window_mask == label)
	valid_mask = init_mask | (curr_window_mask == 0)
	seg, phi, its = CV.prob_chanvese(curr_window/255, init_mask, valid_mask, 10, 0.2)

	new_seg, ns_count = ndimage.label(seg)
	lus, lcs = np.unique(new_seg, return_counts=True)
	delete = [lu for (lu, lc) in zip(lus, lcs) if lc < props['size_filter_add']]
	new_seg[np.isin(new_seg, delete)] = 0
	
	boundaries = ndimage.grey_dilation(new_seg, footprint=np.ones((7, 7)))-ndimage.grey_dilation(new_seg, footprint=np.ones((5, 5)))
	lus, lcs = np.unique(new_seg, return_counts=True)
	if len(lus) == 1:
		mask[wstartx:wendx, wstarty:wendy][curr_window_mask == label] = 0
		return False
	bus, bcs = np.unique(boundaries, return_counts=True)
	mask_means = ndimage.mean(curr_window, new_seg, lus)
	bound_means = ndimage.mean(curr_window, boundaries, bus)
	
	best_match, best_contrast = lus[0], 0
	
	for idx, (lu, lc) in enumerate(zip(lus, lcs)):
		if len(bound_means[bus==lu]) == 0:
			contrast = 0
		else:
			contrast = mask_means[idx]-bound_means[bus==lu][0]
		if contrast > best_contrast:
			best_match = lu
			best_contrast = contrast
	if best_contrast > props["contrast_thresh"]:
		mask[wstartx:wendx, wstarty:wendy][curr_window_mask == label] = 0
		mask[wstartx:wendx, wstarty:wendy][new_seg==best_match] = label
		return True
	else:
		mask[wstartx:wendx, wstarty:wendy][curr_window_mask == label] = 0
		return False