import numpy as np
import cv2
import os
import shutil
import sys
from scipy import ndimage
from skimage import morphology
import json
import modules.util as util
import datetime
	
def label_tracklets(det, tra):
	'''
	Use the tracking data to re-label the cells in every frame.
	This function ensures that tracklet elements all share the same label across multiple images.
	'''
	tracklets = {}
	max_track = 1
	for im in det:
		for l in im:
			if l in tra.keys():
				parent, conn_type = tra[l]
				p_track = tracklets.get(parent, '')
				if p_track == '' or conn_type == 0:
					tracklets[l] = max_track
					max_track += 1
				else:
					tracklets[l] = p_track
			else:
				tracklets[l] = max_track
				max_track += 1
	tracklet_elements = {}
	for l, t in tracklets.items():
		tracklet_elements.setdefault(t, []).append(l)
	return tracklets, tracklet_elements
	
def generate_tra_file(tracklets, tracklet_elements, connections, exponent):
	'''
	Generate the man_track.txt file with the
	[label] [start] [end] [parent]
	format used in the CTC.
	'''
	lines = []
	for t, elems in tracklet_elements.items():
		min_elem = np.min(elems)
		start = min_elem//exponent
		end = np.max(elems)//exponent
		parent = 0
		try:
			p, d = connections[min_elem]
			if d == 0:
				parent = tracklets[p]
			else:
				pass #What?
		except KeyError:
			pass
		lines.append(f'{t} {start} {end} {parent}\n')
	return lines
	
def filter_tracklets(labels, sizes, contrast, cont_thresh, tracklets, tracklet_elements, connections, exponent):
	'''
	Filter out false negative tracklets by using several criteria.
	Currently only size is used, but smoothness and circularity are also available in the development version.
	'''
	to_delete = []
	thresh_upper = 300
	thresh_lower = 120
	for t, elems in tracklet_elements.items():
		mask = np.isin(labels, elems) 
		ss = sizes[mask]
		cons = contrast[mask]
		if np.any(ss < thresh_lower) or np.all(ss < thresh_upper) or np.all(cons < cont_thresh):
			to_delete.append(t)
		
	for t in to_delete:
		for l in tracklet_elements[t]:
			tracklets[l] = 0
		del tracklet_elements[t]
	return tracklets, tracklet_elements
	
def remove_borders(labels, props, idx):
	'''
	Erode the image RoI by a given amount to filter out cells that are only partially in the frame.
	'''
	labels2 = labels.copy()
	boundary = props['boundary']
	labels2[:boundary+1, :] = 0
	labels2[-boundary:, :] = 0
	labels2[:, :boundary+1] = 0
	labels2[:, -boundary:] = 0
	lu = np.unique(labels2)
	
	labels[~np.isin(labels, lu)] = 0
	return labels
		
if len(sys.argv) != 3:
	print('The script should take exactly 3 arguments.')
	exit(1)
		
with open(sys.argv[1], 'r') as file:
	props = json.load(file)
	
props['subset'] = sys.argv[2]

		
if __name__ == '__main__':

	paths_to_create = [
	os.path.join('..', props["dataset"], f'{props["subset"]}_RES')
	]
	for p in paths_to_create:
		if not os.path.exists(p):
			os.mkdir(p)
			
			
	connections = {}
	with open(os.path.join('..', props["dataset"], f'{props["subset"]}_tracking', 'conns.txt'), 'r') as file:
		lines = file.readlines()
		for line in lines:
			num, parent, direct = line.split(' ')
			connections[int(num)] = (int(parent), int(direct))
	all_items = list(connections.keys())
	all_items.extend([v[0] for v in list(connections.values())])
	all_items = np.unique(all_items)
	max_item = np.max(all_items)
	
	fpaths = [p for p in os.listdir(os.path.join('..', props["dataset"], f'{props["subset"]}')) if '.tif' in p]
	first = cv2.imread(os.path.join('..', props["dataset"], f'{props["subset"]}', fpaths[0]), cv2.CV_8UC1)
	imgs = np.zeros((len(fpaths), first.shape[0], first.shape[1]), np.uint8)
	for i in range(len(fpaths)):
		img = cv2.imread(os.path.join('..', props['dataset'], f"{props['subset']}", fpaths[i]), cv2.CV_16UC1)
		imgs[i, :, :] = img
	
	exponent = 10000

	fpaths = [p for p in os.listdir(os.path.join('..', props["dataset"], f'{props["subset"]}_tracking', 'refined')) if '.tif' in p]
	first = cv2.imread(os.path.join('..', props["dataset"], f'{props["subset"]}_tracking', 'refined', fpaths[0]), cv2.CV_8UC1)
	res = np.zeros((len(fpaths), first.shape[0], first.shape[1]), np.uint32)
	for i, fp in enumerate(fpaths):
		print(f'---Reading image {i}---')
		img = cv2.imread(os.path.join('..', props["dataset"], f'{props["subset"]}_tracking', 'refined', fp), cv2.CV_16UC1)
		img = remove_borders(img, props, i)
		res[i] = i*exponent+img

	images = []
	for i in range(res.shape[0]):
		images.append([])
		
	uniques, unique_counts = np.unique(res, return_counts=True)	
	
	for item in uniques:
		img = item//exponent
		if item%exponent == 0:
			continue
		images[img].append(item)
	tracklets, tracklet_elements = label_tracklets(images, connections)
	
	labels_global = []
	sizes_global = []
	contrast_global = []
	start = datetime.datetime.now()
	for i in range(len(fpaths)):
		print(f'---Filtering image {i}---')
		img = imgs[i]
		labels = util.fill_holes(res[i]).astype(np.uint32)
		
		rus, rcs = np.unique(labels, return_counts=True)
		
		boundary = ndimage.grey_dilation(labels, footprint=np.ones((11, 11)))-ndimage.grey_dilation(labels, footprint=np.ones((5, 5)))
		boundary[labels%10000 != 0] = 0
		rus, rcs = np.unique(labels, return_counts=True)
		bus, bcs = np.unique(boundary, return_counts=True)
		mask_means = ndimage.mean(img, labels, rus)
		bound_means = ndimage.mean(img, boundary, bus)
		
		locs = ndimage.value_indices(labels)
		
		for idx, (ru, rc) in enumerate(zip(rus, rcs)):
			
			if ru%exponent == 0:
				continue
			
			if len(bound_means[bus==ru%10000]) == 0:
				contrast = 100
			else:
				contrast = mask_means[idx]-bound_means[bus==ru%10000][0]
			
			
			labels_global.append(ru)
			sizes_global.append(rc)
			contrast_global.append(contrast)
	end = datetime.datetime.now()
	print(f'Took {(end-start).total_seconds()*1000}ms')

	labels_global = np.array(labels_global)
	sizes_global = np.array(sizes_global)
	contrast_global = np.array(contrast_global)
	
	tracklets, tracklet_elements = filter_tracklets(labels_global, sizes_global, contrast_global, np.mean(imgs)/2, tracklets, tracklet_elements, connections, exponent)
	
	mapping = np.zeros(np.max(res[-1])+1, np.uint32)
	for l, t in tracklets.items():
		mapping[l] = t
		
	res = mapping[res]
	
	for i in range(len(fpaths)):
		print(f'---Writing image {i}---')
		cv2.imwrite(os.path.join('..', f'{props["dataset"]}', f'{props["subset"]}_RES', f'mask{str(i).zfill(3)}.tif'), util.fill_holes(res[i]).astype(np.uint16))
	tra_file_lines = generate_tra_file(tracklets, tracklet_elements, connections, exponent)
	with open(os.path.join('..', f'{props["dataset"]}', f'{props["subset"]}_RES', 'res_track.txt'), 'w') as file:
		for l in tra_file_lines:
			file.write(l)

		