import os
import shutil
import sys
import json

if len(sys.argv) != 3:
	print('The script should take exactly 3 arguments.')
	exit(1)
		
with open(sys.argv[1], 'r') as file:
	props = json.load(file)
	
props['subset'] = sys.argv[2]

paths_to_delete = [
os.path.join('..', props["dataset"], f'{props["subset"]}_preproc'),
os.path.join('..', props["dataset"], f'{props["subset"]}_tracking')
]
for p in paths_to_delete:
	if os.path.exists(p):
		shutil.rmtree(p)