#!/bin/bash
# Run the detection, segmentation and tracking for Fluo-N2DH-GOWT1, subset 01
# Prerequisites: Python 3.10.9, CUDA 11.7, packages are listed in requirements.txt

python3 detect.py Fluo-N2DH-GOWT1.json 01
python3 attract.py Fluo-N2DH-GOWT1.json 01
python3 track.py Fluo-N2DH-GOWT1.json 01
python3 cleanup.py Fluo-N2DH-GOWT1.json 01