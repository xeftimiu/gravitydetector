import numpy as np
import cv2
import os
import shutil
import sys
import json
from scipy import ndimage
from skimage import morphology
import modules.detection as detection
import modules.kuwahara
import modules.util as util
import concurrent.futures
import datetime
	
def preprocess(frame, r=6):
	'''Preprocess images using a log transform and kuwahara filter.'''
	
	frame = ((np.log(frame.astype(np.uint16)+1)/(np.log(256))))
	frame = (frame-np.min(frame))/(np.max(frame)-np.min(frame))
	frame = (255*frame).astype(np.uint8)
	frame = modules.kuwahara.Kuwahara_single(frame, N=16, r=r)
	return frame
		
if len(sys.argv) != 3:
	print('The script should take exactly 3 arguments.')
	exit(1)
		
with open(sys.argv[1], 'r') as file:
	props = json.load(file)
	
props['subset'] = sys.argv[2]

fpaths = [p for p in os.listdir(os.path.join('..', props["dataset"], props["subset"])) if '.tif' in p]

def process_one(i, fp):
	'''
	Entry point function for the threadpool executor.
	Can be run sequentially by setting the number of threads to 1.
	'''
	print(f'---Extracting basins for image {i}---')
	start = datetime.datetime.now()
	img_og = cv2.imread(os.path.join('..', props["dataset"], props["subset"], fp), cv2.CV_16UC1)
	img = util.fill_holes(preprocess(img_og, r=4))
	img[img > props["suppress_highs"]] *= 0.75
	
	gravdet = detection.GravityDetector(img.shape, props)
	basins, minima = gravdet.basin_extraction(img, props)

	cv2.imwrite(os.path.join('..', props["dataset"], f'{props["subset"]}_tracking', 'basins', f'mask{str(i).zfill(3)}.tif'), basins.astype(np.uint16))
	cv2.imwrite(os.path.join('..', props["dataset"], f'{props["subset"]}_tracking', 'minima', f'mask{str(i).zfill(3)}.tif'), minima.astype(np.uint16))
	end = datetime.datetime.now()
	print(f'---Image {i} detection done in {(end-start).total_seconds()*1000}ms---')

if __name__ == '__main__':
	paths_to_create = [
	os.path.join('..', props["dataset"], f'{props["subset"]}_tracking'),
	os.path.join('..', props["dataset"], f'{props["subset"]}_tracking', 'basins'),
	os.path.join('..', props["dataset"], f'{props["subset"]}_tracking', 'minima'),
	]
	for p in paths_to_create:
		if not os.path.exists(p):
			os.mkdir(p)

	executor = concurrent.futures.ProcessPoolExecutor(props['num_threads'])
	futures = [executor.submit(process_one, i, fp) for i, fp in enumerate(fpaths)]
	concurrent.futures.wait(futures)