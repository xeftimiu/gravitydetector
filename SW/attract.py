import numpy as np
import os
import sys
import cv2
import json
from scipy import ndimage
import modules.kuwahara
import modules.chan_vese as CV
import modules.util as util
import datetime


def overlap(img1, img2):
	'''
	Calculate the degree of overlap between two sets of labels
	The first image is multiplied by a sufficiently large number so that positional notation
	can be used to create unique pairs of values.
	'''
	mult = 10**(np.floor(np.log10(np.max(img2)))+1)
	overlaps = img1.astype(np.uint64)*mult + img2
	unique_overlaps, counts_overlaps = np.unique(overlaps, return_counts=True)
	overlap_matrix = np.zeros((np.max(img1)+1, np.max(img2)+1))
	for uo, co in zip(unique_overlaps, counts_overlaps):
		label1 = int(uo//mult)
		label2 = int(uo%mult)
		overlap_matrix[label1, label2] = co
	
	return overlap_matrix
	
def remap_basins(basins, minima, mask):
	'''
	Assign the same label to every basin that shares its minimum with the same segmentation mask.
	This is used in the tracking step.
	'''
	ol = overlap(mask, minima)
	ol[0, :] = 0
	ol[:, 0] = 0
	mapping = np.zeros(np.max(basins)+1, np.uint16)
	mapping2 = np.zeros(np.max(basins)+1, np.uint16)
	for m in np.unique(mask):
		if m == 0:
			continue
		nz = np.nonzero(ol[m])[0]
		if nz.shape[0] != 0:
			mapping[nz] = np.min(nz)
			mapping2[nz] = m
	for i in range(mapping.shape[0]):
		if mapping[i] == 0:
			mapping[i] = i
	basins = mapping[basins]
	
	return basins, mapping2
	
def fill_borders(labels):
	'''
	After remapping the basins, the watershed lines must be filled in to allow for accurate tracking.
	Every pixel where every neighbour has the same (non-zero) value can be filled in with that value.
	Notably, this expands the masks by one pixel in every direction.
	'''
	neighs = np.zeros((8, labels.shape[0], labels.shape[1]), np.uint16)
	n = 0
	for i in range(-1, 2):
		for j in range(-1, 2):
			if i == 0 and j == 0:
				continue
			kernel = np.zeros((3, 3))
			kernel[i+1, j+1] = 1
			neighs[n] = ndimage.convolve(labels, kernel, mode='constant', cval=0)
			n += 1
	maxes = np.max(neighs, axis=0)
	neighs[neighs == 0] = 65535
	mins = np.min(neighs, axis=0)
	mask = maxes == mins

	labels[mask] = maxes[mask]
	return labels
				
def find_missing(props, label, basins, curr_basins, prev_basins, curr_mask, prev_mask, curr_img, prev_img, curr_minima, prev_minima, curr_indices, prev_indices, curr_basin_locs, prev_basin_locs):
	'''
	If a cell has no match in either direction, surrounding locations are segmented to possibly locate the missing detection.
	'''
	curr_li = curr_indices[label]
	bb = (np.min(curr_li[0]), np.max(curr_li[0]), np.min(curr_li[1]), np.max(curr_li[1]))
	w, h = bb[1]-bb[0], bb[3]-bb[2]
	cx, cy = bb[0]+w//2, bb[2]+h//2
	xoff, yoff = int(1.5*w/2),  int(1.5*h/2)
	wstartx, wendx = max(0, cx-xoff), min(curr_img.shape[0], cx+xoff)
	wstarty, wendy = max(0, cy-yoff), min(curr_img.shape[1], cy+yoff)
	if wstartx == wendx or wstarty == wendy:
		return False, (-1, [], [])
	curr_window = curr_img[wstartx:wendx, wstarty:wendy]
	curr_window_mask = curr_mask[wstartx:wendx, wstarty:wendy]
	prev_window = prev_img[wstartx:wendx, wstarty:wendy]
	prev_window_mask = prev_mask[wstartx:wendx, wstarty:wendy]
	init_mask = (curr_window_mask == label)
	valid_mask = (prev_window_mask == 0)
	seg, phi, its = CV.prob_chanvese(prev_window/255, init_mask, valid_mask, w, 0.2)
	
	new_seg, ns_count = ndimage.label(seg)
	lus, lcs = np.unique(new_seg, return_counts=True)
	delete = [lu for (lu, lc) in zip(lus, lcs) if lc < props['size_filter_add']]
	new_seg[np.isin(new_seg, delete)] = 0

	boundaries = ndimage.grey_dilation(new_seg, footprint=np.ones((7, 7)))-ndimage.grey_dilation(new_seg, footprint=np.ones((5, 5)))
	lus, lcs = np.unique(new_seg, return_counts=True)
	if len(lus) == 1:
		return False, (-1, [], [])
	bus, bcs = np.unique(boundaries, return_counts=True)
	mask_means = ndimage.mean(prev_window, new_seg, lus)
	bound_means = ndimage.mean(prev_window, boundaries, bus)
	circos = util.circularity(new_seg)
	
	best_match, best_contrast, best_size = lus[0], 0, lcs[0]
	
	for idx, (lu, lc) in enumerate(zip(lus, lcs)):
		if len(bound_means[bus==lu]) == 0:
			contrast = 0
		else:
			contrast = mask_means[idx]-bound_means[bus==lu][0]
		if contrast > best_contrast:
			best_match = lu
			best_contrast = contrast
			best_size = lc
	if best_contrast > props["contrast_thresh"] and not (circos.get(best_match, 0.0) > props["circo_thresh"] or best_size < props["circo_ignore"]):
		print('New cell found')
		new_idx = np.max(prev_mask)+1
		prev_mask[wstartx:wendx, wstarty:wendy][new_seg==best_match] = new_idx
		return True, (label, [], [new_idx])
	else:
		return False, (-1, [], [])

def track_forward(props, curr_basins, next_basins, curr_basins_og, next_basins_og, curr_mask, next_mask, curr_img, next_img, curr_mapping, next_mapping, curr_minima, next_minima, frame_idx, export=False):
	'''
	Perform tracking in forward time.
	In the current version, only missing cell detection is performs, and this 
	function is therefore identical to the backward tracking function.
	'''
	
	overlap_matrix = overlap(curr_mask, next_basins)
	matching = []
	labels = np.unique(curr_mask)
	for label in labels:
		if label == 0:
			continue
		nzs = np.nonzero(overlap_matrix[label])[0]
		if nzs.shape[0] == 0:
			#This shouldn't happen
			print(f'Zero overlap on label {label}')
		else:
			next_matches = next_mapping[nzs]
			uniques = np.unique(next_matches[next_matches != 0])
			matching.append((label, nzs, uniques))
			
	og_overlap_matrix = overlap(curr_mask, next_basins_og)
	og_matching = []
	for label in labels:
		if label == 0:
			continue
		nzs = np.nonzero(og_overlap_matrix[label])[0]
		og_matching.append((label, nzs))
		
	curr_indices = ndimage.value_indices(curr_mask)
	next_indices = ndimage.value_indices(next_mask)
	curr_basin_locs = ndimage.value_indices(curr_minima)
	next_basin_locs = ndimage.value_indices(next_minima)

	next_mask_og = next_mask.copy()
	new_matching = []
	for l, bs, ms in matching:
		if ms.shape[0] > 1:
			new_matching.append((l, bs, ms))
			continue
			
		elif ms.shape[0] == 1:
			new_matching.append((l, bs, ms))
		else:
			retval, (l2, _, ms2) = find_missing(props, l, bs, curr_basins, next_basins, curr_mask, next_mask, curr_img, next_img, curr_minima, next_minima, curr_indices, next_indices, curr_basin_locs, next_basin_locs)
			new_matching.append((l2, _, ms2))
	matching = new_matching.copy()

def track_backward(props, curr_basins, prev_basins, curr_mask, prev_mask, curr_img, prev_img, curr_mapping, prev_mapping, curr_minima, prev_minima, frame_idx, export=False):
	'''
	Perform tracking in reverse time.
	In the current version, only missing cell detection is performs, and this 
	function is therefore identical to the forward tracking function.
	'''
	overlap_matrix = overlap(curr_mask, prev_basins)
	matching = []
	labels = np.unique(curr_mask)
	for label in labels:
		if label == 0:
			continue
		nzs = np.nonzero(overlap_matrix[label])[0]
		if nzs.shape[0] == 0:
			#This shouldn't happen
			print(f'Zero overlap on label {label}')
		else:
			prev_matches = prev_mapping[nzs]
			uniques = np.unique(prev_matches[prev_matches != 0])
			matching.append((label, nzs, uniques))
		
	curr_indices = ndimage.value_indices(curr_mask)
	prev_indices = ndimage.value_indices(prev_mask)
	curr_basin_locs = ndimage.value_indices(curr_minima)
	prev_basin_locs = ndimage.value_indices(prev_minima)

	prev_mask_og = prev_mask.copy()
	new_matching = []
	for l, bs, ms in matching:
		if ms.shape[0] > 1:
			new_matching.append((l, bs, ms))
			continue
			
		elif ms.shape[0] == 1:
			new_matching.append((l, bs, ms))
		else:
			retval, (l2, _, ms2) = find_missing(props, l, bs, curr_basins, prev_basins, curr_mask, prev_mask, curr_img, prev_img, curr_minima, prev_minima, curr_indices, prev_indices, curr_basin_locs, prev_basin_locs)
			new_matching.append((l2, _, ms2))
	matching = new_matching.copy()
	
def track_final(props, curr_basins, prev_basins, curr_mask, prev_mask, curr_img, prev_img, curr_mapping, prev_mapping, curr_minima, prev_minima, frame_idx, connections, export=False):
	'''
	Associate cells and construct tracklets in reverse time.
	Though no explicit mitosis handling is present in the current version,
	this function has to be used in reverse time to allow for multiple
	matches between daughter and parent cells.
	'''
	overlap_matrix = overlap(curr_mask, prev_minima)
	matching = []
	labels = np.unique(curr_mask)
	for label in labels:
		if label == 0:
			continue
		nzs = np.nonzero(overlap_matrix[label])[0]
		if nzs.shape[0] == 0:
			#This shouldn't happen
			print(f'Zero overlap on label {label}')
		else:
			prev_matches = prev_mapping[nzs]
			uniques = np.unique(prev_matches[prev_matches != 0])
			matching.append((label, nzs, uniques))
		
	curr_indices = ndimage.value_indices(curr_mask)
	prev_indices = ndimage.value_indices(prev_mask)
	curr_basin_locs = ndimage.value_indices(curr_minima)
	prev_basin_locs = ndimage.value_indices(prev_minima)

	prev_mask_og = prev_mask.copy()
	new_matching = []
	for l, bs, ms in matching:
		if ms.shape[0] > 1:
			lxs, lys = curr_indices[l]
			mean_x, mean_y = np.mean(lxs), np.mean(lys)
			min_dist = curr_img.shape[0]
			min_m = ms[0]
			for m in ms:
				mxs, mys = prev_indices[m]
				mean_x2, mean_y2 = np.mean(mxs), np.mean(mys)
				dist = np.sqrt((mean_x-mean_x2)**2 + (mean_y-mean_y2)**2)
				if dist < min_dist:
					min_dist = dist
					min_m = m
				
			
			new_matching.append((l, bs, [min_m]))
			
		elif ms.shape[0] == 1:
			new_matching.append((l, bs, ms))
	matching = new_matching.copy()
	
	inv_matching = {}
	for l, bs, ms, in matching:
		for m in ms:
			inv_matching.setdefault(m, []).append(l)
	
	for m, ls in inv_matching.items():
		for l in ls:
			connections[frame_idx*10000+l] = ((frame_idx-1)*10000+m, len(ls) == 1)
	return connections
	
def interpolate_mask(mask1, mask2, alpha=0.5):
	'''
	Binary mask interpolation function based on the work of Iwanowski (see: 10.1007/1-4020-4179-9_90)
	The two masks should have the same image size, but the mask pixels themselves can be distributed in an arbitrary way.
	'''
	xs1, ys1 = np.nonzero(mask1)
	xs2, ys2 = np.nonzero(mask2)
	xs = min(np.min(xs1), np.min(xs2))
	xe = max(np.max(xs1), np.max(xs2))
	ys = min(np.min(ys1), np.min(ys2))
	ye = max(np.max(ys1), np.max(ys2))
	
	maskR = np.zeros_like(mask1)
	maskR[xs:xe+1, ys:ye+1] = 1

	
	SR = np.count_nonzero(maskR)
	SM1 = np.count_nonzero(mask1)
	SM2 = np.count_nonzero(mask2)
	
	dist1 = ndimage.distance_transform_edt(1-mask1)[xs:xe+1, ys:ye+1]
	dist1c = ndimage.distance_transform_edt(maskR)[xs:xe+1, ys:ye+1]
	int1 = (255*dist1/(dist1+dist1c)).astype(np.uint16)
	#int1[maskR==0] += 1
	u1, c1 = np.unique(int1, return_counts=True)
	SI1 = np.zeros(np.max(u1)+1)
	SI1[u1] = np.cumsum(c1)
	
	dist2 = ndimage.distance_transform_edt(1-mask2)[xs:xe+1, ys:ye+1]
	dist2c = dist1c.copy()
	int2 = (255*dist2/(dist2+dist2c)).astype(np.uint16)
	#int2[maskR==0] += 1
	u2, c2 = np.unique(int2, return_counts=True)
	SI2 = np.zeros(np.max(u2)+1)
	SI2[u2] = np.cumsum(c2)
	
	N1 = (SI1[int1]-SM1)/(SR-SM1)
	N2 = (SI2[int2]-SM2)/(SR-SM2)
	
	int1T = N1 <= alpha
	int2T = N2 <= (1-alpha)
	new_mask = np.zeros_like(maskR)
	new_mask[xs:xe+1, ys:ye+1] = int1T & int2T
	return new_mask
	
def anti_blink(props, prev_mask, curr_mask, next_mask, curr_img):
	'''
	Use mask interpolation to detect cells that are found in the 
	past and future frames, but not the current frame.
	'''
	pm = ndimage.grey_erosion(prev_mask, footprint=np.ones((3, 3)))
	cm = ndimage.grey_erosion(curr_mask, footprint=np.ones((3, 3)))
	nm = ndimage.grey_erosion(next_mask, footprint=np.ones((3, 3)))
	c_overlap_matrix = overlap(pm, cm)
	n_overlap_matrix = overlap(pm, nm)

	labels = np.unique(prev_mask)
	for label in labels:
		if label == 0:
			continue
		cnzs = np.nonzero(c_overlap_matrix[label])[0]
		cnzs = cnzs[cnzs != 0] 
		nnzs = np.nonzero(n_overlap_matrix[label])[0]
		nnzs = nnzs[nnzs != 0]
		if len(nnzs) == 1 and len(cnzs) == 0:
			match = nnzs[0]
			mask1 = prev_mask == label
			mask2 = next_mask == match	
			new_mask = interpolate_mask(mask1, mask2)
			util.CV_refine(props, curr_img, new_mask, 1)
			curr_mask[new_mask != 0] = np.max(labels)+1
	return curr_mask
		
if len(sys.argv) != 3:
	print('The script should take exactly 3 arguments.')
	exit(1)
		
with open(sys.argv[1], 'r') as file:
	props = json.load(file)
	
props['subset'] = sys.argv[2]
		
if __name__ == '__main__':
	paths_to_create = [
	os.path.join('..', props["dataset"], f'{props["subset"]}_tracking', 'refined'),
	]
	for p in paths_to_create:
		if not os.path.exists(p):
			os.mkdir(p)

	fpaths = [p for p in os.listdir(os.path.join('..', props["dataset"], f'{props["subset"]}_preproc')) if '.tif' in p]
	first = cv2.imread(os.path.join('..', props["dataset"], f'{props["subset"]}_preproc', fpaths[0]), cv2.CV_8UC1)
	imgs = np.zeros((len(fpaths), first.shape[0], first.shape[1]), np.uint8)
	for i in range(len(fpaths)):
		img = cv2.imread(os.path.join('..', props['dataset'], f"{props['subset']}_preproc", fpaths[i]), cv2.CV_16UC1)
		imgs[i, :, :] = img
	
	basins_dir = os.path.join('..', props["dataset"], f'{props["subset"]}_tracking', 'basins')
	minima_dir = os.path.join('..', props["dataset"], f'{props["subset"]}_tracking', 'minima')
	masks_dir = os.path.join('..', props["dataset"], f"{props['subset']}_tracking", 'raw')
	basins = np.zeros_like(imgs, np.uint16)
	minima = np.zeros_like(imgs, np.uint16)
	masks = np.zeros_like(imgs, np.uint16)
	for i, fpath in enumerate(os.listdir(basins_dir)):
		basins[i] = cv2.imread(os.path.join(basins_dir, fpath), cv2.CV_16UC1)
	for i, fpath in enumerate(os.listdir(minima_dir)):
		minima[i] = cv2.imread(os.path.join(minima_dir, fpath), cv2.CV_16UC1)
	for i, fpath in enumerate(os.listdir(masks_dir)):
		masks[i] = cv2.imread(os.path.join(masks_dir, fpath), cv2.CV_16UC1)

	start = datetime.datetime.now()
	for i in range(len(fpaths)-1, 0, -1):
		print(f'---Backward pass on image {i}---')
		curr_minima, prev_minima = minima[i], minima[i-1]
		curr_basins, prev_basins = basins[i], basins[i-1]
		curr_mask, prev_mask = masks[i], masks[i-1]
		curr_img, prev_img = imgs[i].astype(np.uint8), imgs[i-1].astype(np.uint8)

		curr_basins, curr_mapping = remap_basins(curr_basins, curr_minima, curr_mask)
		prev_basins, prev_mapping = remap_basins(prev_basins, prev_minima, prev_mask)
		
		curr_basins = fill_borders(curr_basins)
		prev_basins = fill_borders(prev_basins)
	
		track_backward(props, curr_basins, prev_basins, curr_mask, prev_mask, curr_img, prev_img, curr_mapping, prev_mapping, curr_minima, prev_minima, i)
		masks[i-1] = prev_mask
		
	end = datetime.datetime.now()
	print(f'Took {(end-start).total_seconds()*1000}ms')
	for i in range(0, len(fpaths)-1):
		print(f'---Forward pass on image {i}---')
		prev_minima, curr_minima = minima[i], minima[i+1]
		prev_basins, curr_basins = basins[i], basins[i+1]
		prev_mask, curr_mask = masks[i], masks[i+1]
		prev_img, curr_img = imgs[i].astype(np.uint8), imgs[i+1].astype(np.uint8)
		
		curr_basins_map, curr_mapping = remap_basins(curr_basins, curr_minima, curr_mask)
		prev_basins_map, prev_mapping = remap_basins(prev_basins, prev_minima, prev_mask)
		
		curr_basins = fill_borders(curr_basins)
		prev_basins = fill_borders(prev_basins)
		track_forward(props, prev_basins_map, curr_basins_map, prev_basins, curr_basins, prev_mask, curr_mask, prev_img, curr_img, prev_mapping, curr_mapping, prev_minima, curr_minima, i, False)
		masks[i+1] = curr_mask


	cv2.imwrite(os.path.join('..', f'{props["dataset"]}', f'{props["subset"]}_tracking', 'refined', f'mask{str(0).zfill(3)}.tif'), masks[0].astype(np.uint16))
	for i in range(1, len(fpaths)-1):
		print(f'---De-blinking image {i}---')
		prev_mask, curr_mask, next_mask = masks[i-1], masks[i], masks[i+1]
		curr_img = imgs[i].astype(np.uint8)
		new_curr = anti_blink(props, prev_mask, curr_mask, next_mask, curr_img)
		masks[i] = new_curr
		cv2.imwrite(os.path.join('..', f'{props["dataset"]}', f'{props["subset"]}_tracking', 'refined', f'mask{str(i).zfill(3)}.tif'), masks[i].astype(np.uint16))
	cv2.imwrite(os.path.join('..', f'{props["dataset"]}', f'{props["subset"]}_tracking', 'refined', f'mask{str(i+1).zfill(3)}.tif'), masks[-1].astype(np.uint16))
	
	
	connections = {}
	for i in range(len(fpaths)-1, 0, -1):
		print(f'---Final pass on image {i}---')
		curr_minima, prev_minima = minima[i], minima[i-1]
		curr_basins, prev_basins = basins[i], basins[i-1]
		curr_mask, prev_mask = masks[i], masks[i-1]
		curr_img, prev_img = imgs[i].astype(np.uint8), imgs[i-1].astype(np.uint8)

		curr_basins, curr_mapping = remap_basins(curr_basins, curr_minima, curr_mask)
		prev_basins, prev_mapping = remap_basins(prev_basins, prev_minima, prev_mask)
		
		curr_basins = fill_borders(curr_basins)
		prev_basins = fill_borders(prev_basins)
	
		connections = track_final(props, curr_basins, prev_basins, curr_mask, prev_mask, curr_img, prev_img, curr_mapping, prev_mapping, curr_minima, prev_minima, i, connections)
		
	with open(os.path.join('..', f'{props["dataset"]}', f'{props["subset"]}_tracking', 'conns.txt'), 'w') as file:
		for c, (p, d) in connections.items():
			file.write(f'{c} {p} {int(d)}\n')
		