import numpy as np
import cv2
import os
import shutil
import sys
import json
from scipy import ndimage
from skimage import morphology, segmentation
import modules.mask_extraction as extraction
import modules.kuwahara
import modules.util as util
import concurrent.futures
import datetime

def get_seeds(labels, minima, thresh):
	'''Get the minima locations that correspond to basins above [thresh] size.'''
	us, cs = np.unique(labels, return_counts=True)
	keep = []
	for u, c in zip(us, cs):
		if u == 0:
			continue
		if c > thresh:
			keep.append(u)
	xs, ys = np.nonzero(np.isin(minima, keep))
	return xs, ys
	
def preprocess(frame, r=6, clip=20):
	'''Enhance and filter images using CLAHE and kuwahara filter'''
	clahe = cv2.createCLAHE(clip, (16, 16))
	frame = clahe.apply(frame)
	frame = modules.kuwahara.Kuwahara_single(frame, N=16, r=r)
	
	return frame
		
		
if len(sys.argv) != 3:
	print('The script should take exactly 3 arguments.')
	exit(1)
		
with open(sys.argv[1], 'r') as file:
	props = json.load(file)
	
props['subset'] = sys.argv[2]

fpaths = [p for p in os.listdir(os.path.join('..', props["dataset"], props["subset"])) if '.tif' in p]
bpaths = [p for p in os.listdir(os.path.join('..', props["dataset"], f'{props["subset"]}_tracking', 'basins')) if '.tif' in p]
mpaths = [p for p in os.listdir(os.path.join('..', props["dataset"], f'{props["subset"]}_tracking', 'minima')) if '.tif' in p]


def process_one(i, fp, bp, mp):
	'''
	Entry point function for the threadpool executor.
	Can be run sequentially by setting the number of threads to 1.
	'''
	print(f'---Detecting image {i}---')
	start = datetime.datetime.now()
	img_og = cv2.imread(os.path.join('..', props["dataset"], props["subset"], fp), cv2.CV_16UC1)
	basins = cv2.imread(os.path.join('..', props["dataset"], f'{props["subset"]}_tracking', 'basins', bp), cv2.CV_16UC1)
	minima = cv2.imread(os.path.join('..', props["dataset"], f'{props["subset"]}_tracking', 'minima', mp), cv2.CV_16UC1)
	xs, ys = get_seeds(basins, minima, img_og.size*props['merge_filter'])
		
	img2 = preprocess(img_og, clip=60, r=6)
	img2 = ndimage.grey_dilation(img2, footprint=np.ones((5, 5)))
	img2 = util.fill_holes(img2).astype(np.uint8)
	img2 = ndimage.grey_erosion(img2, footprint=np.ones((5, 5))).astype(np.uint8)
	
	labels = extraction.extract_masks(img2, props, xs=xs, ys=ys).astype(np.uint16)	
	
	lu, lc = np.unique(labels, return_counts=True)
	for u in lu:
		if u == 0:
			continue
		util.CV_refine(props, img2, labels, u)
	lu, lc = np.unique(labels, return_counts=True)
	delete = [u for (u, c) in zip(lu, lc) if c < props['size_filter_lower'] or c > props['size_filter_upper']]
	labels[np.isin(labels, delete)] = 0
	
	dist_trans = ndimage.distance_transform_edt(labels)
	seeds, _ = ndimage.label(ndimage.binary_dilation(morphology.h_maxima(dist_trans, 5), structure=np.ones((9, 9))))

	labels = segmentation.watershed(-dist_trans, seeds, connectivity=2, mask=labels)
	boundaries = ndimage.grey_dilation(labels, footprint=np.ones((7, 7)))-ndimage.grey_dilation(labels, footprint=np.ones((5, 5)))
	locs = ndimage.value_indices(labels)
	
	lus, lcs = np.unique(labels, return_counts=True)
	bus, bcs = np.unique(boundaries, return_counts=True)
	mask_means = ndimage.mean(img2, labels, lus)
	bound_means = ndimage.mean(img2, boundaries, bus)
	
	circos = util.circularity(labels)
	
	delete = []
	for idx, (lu, lc) in enumerate(zip(lus, lcs)):
		if lu == 0:
			continue
			
		area = lc
		circ = circos.get(lu, 0.00)
			
		if len(bound_means[bus==lu]) == 0:
			contrast = 100
		else:
			contrast = mask_means[idx]-bound_means[bus==lu][0]
		crits = [lc < props['size_filter_lower'] or lc > props['size_filter_upper'], contrast < props['contrast_thresh'], circ > props['circo_thresh'] and area > 1500 and contrast < 50]
		if np.any(crits):
			delete.append(lu)
		
	labels[np.isin(labels, delete)] = 0

	cv2.imwrite(os.path.join('..', props["dataset"], f'{props["subset"]}_preproc', f't{str(i).zfill(3)}.tif'), img2.astype(np.uint16))
	cv2.imwrite(os.path.join('..', props["dataset"], f'{props["subset"]}_tracking', 'raw', f'mask{str(i).zfill(3)}.tif'), labels.astype(np.uint16))
	end = datetime.datetime.now()
	print(f'---Image {i} detection done in {(end-start).total_seconds()*1000}ms---')

if __name__ == '__main__':
	paths_to_create = [
	os.path.join('..', props["dataset"], f'{props["subset"]}_preproc'),
	os.path.join('..', props["dataset"], f'{props["subset"]}_tracking', 'raw')
	]
	for p in paths_to_create:
		if not os.path.exists(p):
			os.mkdir(p)

	executor = concurrent.futures.ProcessPoolExecutor(props['num_threads'])
	futures = [executor.submit(process_one, i, fp, bp, mp) for i, (fp, bp, mp) in enumerate(zip(fpaths, bpaths, mpaths))]
	concurrent.futures.wait(futures)
