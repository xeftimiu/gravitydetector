@echo off
REM Run the detection, segmentation and tracking for Fluo-N2DH-GOWT1, subset 02
REM Prerequisites: Python 3.10.9, CUDA 11.7, packages are listed in requirements.txt

python basins.py Fluo-N2DH-GOWT1.json 02
python detect.py Fluo-N2DH-GOWT1.json 02
python attract.py Fluo-N2DH-GOWT1.json 02
python track.py Fluo-N2DH-GOWT1.json 02