# Gravitational cell detector

# Table of contents
1. [Introduction](#introduction)
2. [Instalation](#installation)
3. [Project description](#project-description)
4. [Examples](#examples)
5. [Support](#support)

## 1. Introduction
This repository contains the reference implementation of our paper titled "Gravitational Cell Detection and Tracking in Fluorescence
Microscopy Data". Furthermore, it is a copy of the Cell Tracking Challenge submission cited in said paper.

![process](images/process.PNG)


## 2. Instalation

Prerequisites:
- Windows OS (Linux is theoretically supported but untested)
- NVIDIA GPU with compute capability 7.5+

All required packages are listed in the requirements.txt file. 
PyCUDA may require manual installation, and in some cases, a complete reinstall of CUDA may be necessary.

## 3. Project description<a name="project-description" />

The pipeline consists of 5 primary scripts and several module and utility scripts that cannot be used on their own.
The primary scipts include:
* basins.py - Calculates the basins of attraction and minima used for detection and tracking. These are saved as .tif files under \[dataset name\]/\[video sequence ID\]_tracking/basins and minima.
* detect.py - Performs segmentation using a region growing algorithm based on the basins of attraction. Outputs are saved to \[dataset name\]/\[video sequence ID\]_tracking/raw, with the preprocessed images saved for future use under \[dataset name\]-\[video sequence ID\]_preproc.
* attract.py - Performs rudimentary tracking to correct missing detections and construct partial tracklets. Outputs are saved to \[dataset name\]/\[video sequence ID\]_tracking/refined and conns.txt.
* track.py - Constructs the full tracklets and filters out false detections. Outputs are saved to the CTC-compliant \[dataset name\]/\[video sequence ID\]_RES folder.
* cleanup.py - Deletes all temporary files, including the entire _tracking folder and the _preproc folder.

The modules include the following files:
* chan_vese.py - Contains an implementation of the probabilistic Chan-Vese model usef for refining masks and detecting missing cells.
* detection.py - Implements classes for efficient FFT convolution based on the scipy implementation, as well as the attraction basin calculation and gradient descent.
* euler_heun.py - Serves as a python wrapper for the C++ implementation of the explicit Euler and adaptive Euler-Heun (with or without rasterization) methods.
* kuwahara.py - Serves as the pyCUDA context manager and the dispatcher for the CUDA C++ kernel of the kuwahara filter.
* mask_extraction.py - Wraps around a C++ implementation of the mask extraction function. This file also includes some deprecated functions, including a python implementation of the same algorithm, as well as an older, morphology-based one.
* util.py - A collection of functions used at multiple points of the pipeline. 

All C++ source codes can be found under the modules/source_codes folder, witht he exception of kuwahara_kernel_code.cpp, which must stay in the same folder as kuwahara.py
The project folder should be structured in the following manner:
```
--'DATASET_NAME'								# Name of dataset e.g. 'Fluo-N2DH-GOWT1'                    
	-- YY                           			# YY - name of sequence, a two digit number, e.g. '01', '02'
		-- t000.tif
		-- t001.tif
		  ...
		-- tXXX.tif
		-- YY_GT               
		   -- SEG           					# reference cell segmentation (instance seg. or semantic seg.) e.g. 'SEG'
				-- man_seg000.tif
				-- man_seg001.tif
				   ...
				-- man_segXXX.tif
		   -- TRA          						# reference cell markers e.g. 'TRA'
				-- man_track000.tif
				-- man_track001.tif
				  ...
				-- man_trackXXX.tif
--SW
	-- modules
	-- basins.py
	-- detect.py
	-- attract.py
	-- track.py
	-- cleanup.py
	-- 'DATASET_NAME'.json
	-- 'DATASET_NAME'-YY.bat
	-- 'DATASET_NAME'-YY.sh
```

## 4. Examples

### 4.1. Run segmentation and tracking of Fluo-N2DH-GOWT1

As this repository is a copy of a Cell Tracking Challenge submission, scripts for running the pipeline for the Fluo-N2DH-GOWT1 dataset are already included.
The \[dataset name\]-\[video sequence ID\].bat and .sh files will automatically run the entire pipeline, including cleanup.

To run only individual scripts (such as for debugging purposes), you can use a command line interface.
All scripts take exactly two arguments: a config json file and a video sequence ID.

### 4.2. Create a config file for a new dataset

To run the pipeline on other CTC datasets, it is recommended to create new config files.
The provided .json file for Fluo-N2DH-GOWT1 can be used as a template, and different configs may be created for each individial video sequence.

| Argument          | Description                                                         | Accepted values       | Default value   |
|-------------------|---------------------------------------------------------------------|-----------------------|-----------------|
| dataset           | Dataset to process                                                  | string                | Fluo-N2DH-GOWT1 |
| mask_method       | What method to use for segmentation                                 | cpp, heap, morphology | cpp             |
| gravity_mult      | Constant multiplier for the strength of gravity                     | float                 | 1               |
| distance_mult     | Distance between 1.connected neighbour pixels                       | float                 | 1               |
| step_size         | Initial step size for the Euler-Heun method                         | float                 | 0.01            |
| suppress_highs    | Intensity clip limit for the gravity method's preprocessing         | int                   | 150             |
| merge_filter      | The minimal basin size for significant minima                       | float                 | 0.002           |
| boundary          | Size of the image border outside of which cells should be discarded | int                   | 45              |
| wall_thresh_mult  | Local contrast parameter for the segmentation step                  | float                 | 0.001           |
| wall_thresh_halve | Local contrast below which the wall_thresh_mult should be halved    | int                   | 110             |
| size_filter_upper | Hysteresis thresholding upper limit for size                        | int                   | 150000          |
| size_filter_upper | Hysteresis thresholding lower limit for size                        | int                   | 300             |
| size_filter_add   | Minimum size for correcting missed detections                       | int                   | 200             |
| contrast_thresh   | Tracklet filtering contrast threshold                               | float                 | 5.0             |
| circo_thresh      | Tracklet filtering circularity threshold                            | float                 | 0.25            |
| circo_ignore      | Size below which to ignore circularity                              | int                   | 1000            |
| conv_shape        | Use a kernel with a fixed size or convolve the entire image         | fixed, dynamic        | fixed           |
| conv_size         | Convolution kernel size. Only used for conv_shape=fixed.            | int                   | 32              |
| num_threads       | Number of threads to use for basin calculation and detection.       | int                   | 1               |

## 5. Support
The software is under development. If you would like to report a bug or ask for support, please contact us at [eftimiu@mail.muni.cz](eftimiu@mail.muni.cz).

